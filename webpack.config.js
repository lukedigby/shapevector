const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PATHS_TO_TREAT_AS_CSS_MODULES = [
  'react-toolbox',
]
module.exports = {
  entry: './web/assets/src/js/entry.js',
  output: {
    filename: 'bundle.js',
    path: 'web/assets/build/js/',
    publicPath: '/assets/build/js/'       
  },
  resolve: {
    extensions: ['', '.scss', '.js', '.jsx', '.json'],
    modulesDirectories: [
      'node_modules',
      path.resolve(__dirname, './node_modules')
    ]
  },
  module: {
  	loaders: [
	  	{
	  		test: /\.jsx?$/,
	  		include: [
          path.resolve(__dirname, "web/assets/src")
        ], 
	  		loader: 'babel',
	  		query: {
	  			presets: ['es2015', 'react']
        },
        plugins: [
          'react-require'
        ]
      },
      {
        test: /\.(scss|css)$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass?sourceMap')
      }
    ]
  },
  externals: {
    "routing": "Routing"
  },
  postcss: [autoprefixer],
  plugins: [
    new ExtractTextPlugin('spec.css', { allChunks: true })
    /*new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })*/
  ]
};