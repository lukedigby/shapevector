<?php

use AppBundle\Worker\OrderWorkerCommand;
use Symfony\Component\Console\Application;

/**
 * @var Composer\Autoload\ClassLoader
 */
require __DIR__.'/app/autoload.php';

$application = new Application();
$application->add(new OrderWorkerCommand());
$application->run();
	