<?php

namespace AppBundle\RestController;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Post;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Api\Shapeways;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * @RouteResource("ShapewaysModel")
 */
class ShapewaysModelController extends FOSRestController
{
    private $api;

    private function getApi()
    {
        if($this->api == null){
            $userManager = $this->get('fos_user.user_manager');
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $this->api = new Shapeways($this->container, $userManager, $user);
        }

        return $this->api;
    }

    public function postAction(Request $request)
    {
        $file = $request->files->get('file');
        if($request->request->get('uploadScale') == 'mm'){
            $scale = 0.001;
        }else if($request->request->get('uploadScale') == 'm'){
            $scale = 1;
        }
        $logger = $this->get('logger');
        $response = $this->getApi()->uploadModel($file, $scale, $logger);

        $view = $this->view([
            'modelId' => $response->modelId,
            'modelName' => $response->title
        ], 200);
        return $this->handleView($view);
    }
}
