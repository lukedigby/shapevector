<?php

namespace AppBundle\RestController;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Receipt;
use AppBundle\Entity\Transaction;

class TransactionController extends FOSRestController
{
    public function putTransactionAction($transaction_id, Request $request)
    {
        $currentUser = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $transaction = $em->getRepository('AppBundle:Transaction')->find($transaction_id);
        $receipt = $transaction->getReceipt();
        if($currentUser !== $receipt->getUser()){
            return $this->handleView($this->view()->setStatusCode(403));
        }

        $values = $request->request;

        $receipt->setFirstName($values->get('firstName'));
        $receipt->setLastName($values->get('lastName'));
        $receipt->setAddress1($values->get('address1'));
        $receipt->setAddress2($values->get('address2'));
        $receipt->setCity($values->get('city'));
        $receipt->setState($values->get('state'));
        $receipt->setZip($values->get('zip'));
        $receipt->setShapewaysOrderNumber($values->get('shapewaysOrderNumber'));
        if($values->get('targetShippingDate')){
            $targetShippingDate = new \DateTime();
            $targetShippingDate->setTimestamp($values->get('targetShippingDate'));
        }else{
            $targetShippingDate = null;
        }
        $receipt->setTargetShippingDate($targetShippingDate);

        $transaction->setModelId($values->get('modelId'));
        $transaction->setModelName($values->get('modelName'));
        $transaction->setMaterialId($values->get('materialId'));
        $transaction->setOrderNotes($values->get('orderNotes'));
        $transaction->getTags()->clear();
        if($values->has('tags')){
            foreach($values->get('tags') as $id){
                $transaction->addTag($em->getReference('AppBundle\Entity\Tag', $id));
            }
        }

        $em->flush();

        return $this->handleView($this->view()->setStatusCode(204));
    }
}