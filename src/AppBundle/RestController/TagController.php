<?php

namespace AppBundle\RestController;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Tag;

class TagController extends FOSRestController
{
    public function getTagsAction()
    {
        $currentUser = $this->get('security.context')->getToken()->getUser();

        $tags = $this->getDoctrine()->getManager()->createQuery('SELECT DISTINCT tg FROM AppBundle:Tag tg JOIN tg.transactions t JOIN t.receipt r WHERE r.userId = :userId ORDER BY tg.name ASC')->setParameter('userId', $currentUser->getId())->getArrayResult();
        $view = $this->view($tags, 200);
        return $this->handleView($view);
    }

    public function postTagAction(Request $request)
    {
        if(!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            return $this->handleView($this->view()->setStatusCode(403));
        }
        $em = $this->getDoctrine()->getManager();
        $values = $request->request;
        $tag = $em->getRepository('AppBundle:Tag')->findOneBy(['name' => $values->get('name')]);
        if(!$tag){
            $tag = new Tag();
            $tag->setName($values->get('name'));
            $em->persist($tag);
            $em->flush();
        }

        $response = [
            'id' => $tag->getId(),
            'name' => $tag->getName()
        ];

        return $this->handleView($this->view($response, 200));
    }
}
    