<?php

namespace AppBundle\RestController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Process\PhpExecutableFinder;
use FOS\RestBundle\Controller\Annotations\Post;

use AppBundle\Entity\Receipt;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderRequest;
use AppBundle\Entity\Transaction;
use AppBundle\Api\Shopify;
use Symfony\Component\Process\Process;

class ReceiptController extends FOSRestController
{
    public function getReceiptsAction()
    {
        $currentUser = $this->get('security.context')->getToken()->getUser();
        $receipts = $this->getDoctrine()->getManager()->createQuery('SELECT r, t, partial tg.{id} FROM AppBundle:Receipt r LEFT JOIN r.transactions t LEFT JOIN t.tags tg WHERE r.userId = :userId ORDER BY r.orderTime DESC')->setParameter('userId', $currentUser->getId())->getArrayResult();
        foreach($receipts as &$r){
            foreach($r['transactions'] as &$t){
                $tags = [];
                foreach($t['tags'] as $tg){
                    $tags[] = $tg['id'];
                }
                $t['tags'] = $tags;

                $t['extraDetails'] = json_decode($t['extraDetails'], true);
            }
        }
        $view = $this->view($receipts, 200);
        return $this->handleView($view);
    }

    /**
     * @Post("/orderrequests")
     */
    public function postOrderRequestAction()
    {
        $em = $this->getDoctrine()->getManager();
        $orderRequest = new OrderRequest();
        $orderRequest->setUserId($this->getUser()->getId())
            ->setIsProcessing(false)
            ->setIsCompleted(false);
        $em->persist($orderRequest);
        $em->flush();
        $this->triggerWorker();
        //$this->get('session')->set('processing_orders', true);
        //return $this->redirectToRoute('main');
        return $this->handleView($this->view()->setStatusCode(204));
    }


    private function triggerWorker()
    {
        $php = $this->container->getParameter('php_path');
        $rootDir = $this->get('kernel')->getRootDir();
        $cmd = "$php $rootDir/console worker";
        $this->get('logger')->info('Running command: ' . $cmd);
        $this->execInBackground($cmd);
    }

    private function execInBackground($cmd) 
    {
        $process = new Process($cmd);
        $process->start();
        while ($process->isRunning()) {
            // waiting for process to finish
        }

        $this->get('logger')->info($process->getOutput());
        return;
        if (false && substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ". $cmd, "r"));  
        } 
        else {
            $output = [];
            $return_var = null;
            $result = exec($cmd, $output, $return_var);
            $this->get('logger')->info('Worker output: ' . implode("\n", $output));
            $this->get('logger')->info('Worker return var: ' . $return_var);
            $this->get('logger')->info('Worker result: ' . $result);
        }
    } 
}
