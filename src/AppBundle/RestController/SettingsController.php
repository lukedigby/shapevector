<?php

namespace AppBundle\RestController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;

use AppBundle\Entity\TagRule;
use AppBundle\Entity\Tag;

class SettingsController extends FOSRestController
{

  public function getSettingsAction()
  {
    $user = $this->getUser();
    $tag_rules = $this->getDoctrine()->getManager()->createQuery('SELECT r, t FROM AppBundle:TagRule r JOIN r.tag t WHERE r.userId = :userId')->setParameter('userId', $user->getId())->getArrayResult();

    foreach($tag_rules as $key => $rule){
      if($rule['field'] == '' && $rule['value'] == ''){
        $global_rule = $rule;
        array_splice($tag_rules, $key, 1);
      }
    }
    if(!isset($global_rule)){
      $global_rule = [
        'field' => '',
        'value' => '',
        'tag' => ''
      ];
    }

    $response = [
      'tag_rules' => $tag_rules,
      'global_rule' => $global_rule
    ];

    $view = $this->view($response, 200)->setFormat('json');
    return $this->handleView($view);
  }

  public function postTagRuleAction(Request $request)
  {
    $tag_rule = new TagRule();
    $tag_rule->setTag($this->getDoctrine()->getRepository('AppBundle:Tag')->find($request->request->get('tag')['id']));
    $tag_rule->setValue($request->request->get('value'));
    $tag_rule->setField($request->request->get('field'));
    $tag_rule->setUser($this->getUser());

    $this->getDoctrine()->getManager()->persist($tag_rule);
    $this->getDoctrine()->getManager()->flush();

    $response = [
      'id' => $tag_rule->getId(),
      'status' => 'success'
    ];

    $view = $this->view($response, 200)->setFormat('json');
    return $this->handleView($view);
  }

  public function putTagRuleAction(Request $request)
  {
    $tag_rule = $this->getDoctrine()->getRepository('AppBundle:TagRule')->find($request->request->get('id'));
    $tag_rule->setTag($this->getDoctrine()->getRepository('AppBundle:Tag')->find($request->request->get('tag')['id']));
    $tag_rule->setValue($request->request->get('value'));
    $tag_rule->setField($request->request->get('field'));
    $tag_rule->setUser($this->getUser());

    $this->getDoctrine()->getManager()->flush();

    $response = [
      'status' => 'success'
    ];

    $view = $this->view($response, 200)->setFormat('json');
    return $this->handleView($view);
  }

  public function deleteTagRuleAction(Request $request)
  {
    $tag_rule = $this->getDoctrine()->getManager()->getReference('AppBundle:TagRule', $request->request->get('id'));
    $this->getDoctrine()->getManager()->remove($tag_rule);

    $this->getDoctrine()->getManager()->flush();

    $response = [
      'status' => 'success'
    ];

    $view = $this->view($response, 200)->setFormat('json');
    return $this->handleView($view);
  }
}