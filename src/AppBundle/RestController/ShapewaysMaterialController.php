<?php

namespace AppBundle\RestController;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Post;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Api\Shapeways;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * @RouteResource("ShapewaysMaterial")
 */
class ShapewaysMaterialController extends FOSRestController
{
    private $api;

    private function getApi()
    {
        if($this->api == null){
            $userManager = $this->get('fos_user.user_manager');
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $this->api = new Shapeways($this->container, $userManager, $user);
        }

        return $this->api;
    }

    public function cgetAction()
    {
        $response = $this->getApi()->getMaterials($this->get('logger'));
        $materials = json_encode(json_decode($response)->materials);
        $view = $this->view(['materials' => $materials], 200);
        return $this->handleView($view);
    }
}
