<?php

namespace AppBundle\RestController;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Post;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Api\Shapeways;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * @RouteResource("ShapewaysOrder")
 */
class ShapewaysOrderController extends FOSRestController
{
    private $api;

    private function getApi()
    {
        if($this->api == null){
            $userManager = $this->get('fos_user.user_manager');
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $this->api = new Shapeways($this->container, $userManager, $user);
        }

        return $this->api;
    }

    public function postAction(Request $request)
    {
        $logger = $this->get('logger');
        $response = $this->getApi()->postOrder($request, $logger);
        $logger->info('Post Order: '.print_r($response, true));
        
        $view = $this->view(['response' => $response], 200);
        return $this->handleView($view);
    }

    public function getAction($id)
    {
        $response = $this->getApi()->getOrder($id);
        echo "<pre>";
        print_r(json_decode($response));
        
        $view = $this->view(['response' => $response], 200);
        return $this->handleView($view);
    }

    public function cgetAction()
    {
        $response = $this->getApi()->getOrders($this->get('logger'));
        print_r(json_decode($response));

        $view = $this->view(['response' => $response], 200);
        return $this->handleView($view);
    }

    public function putAction($id, Request $request)
    {
        if($request->request->get('status') != 'cancelled'){
            return $this->handleView($this->view([], 400));
        }
        $response = $this->getApi()->cancelOrder($id);
        $this->get('logger')->info('Cancelled: '.print_r($response, true));
        
        $view = $this->view($response, 200);
        return $this->handleView($view);
    }

    public function getShippingStatusAction($ids)
    {

        $view = $this->view($response, 200);
        return $this->handleView($view);
    }

}
