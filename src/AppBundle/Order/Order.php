<?php
namespace AppBundle\Order;

use AppBundle\Entity\Receipt;
use Doctrine\ORM\EntityManager;

class Order
{
	private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * Fetch orders from the database to display
	 */
	public function getOrders(Request $request)
	{
		$this->limit = isset($_GET['limit']) ? (int)$_GET['limit'] : 25;
		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
		$this->offset = ($page - 1) * $this->limit;

		$where_clause = $this->createWhereClause();
    $transactions = $this->getTransactions($page, $where_clause);
    $orders = $this->getReceipts($transactions);
    $orders->pagination = $this->getPagination($where_clause);

		return $orders;
	}

  /**
   * Creates search parameters from GET query
   *
   * @return string
   */
  private function createWhereClause(Request $request)
	{
		$where_clause = Array();

		if(isset($_GET['min_date']) && $_GET['min_date'] != ''){
			$where_clause[] = ' r.order_time >= ' . strtotime($_GET['min_date']);
		}

		if(isset($_GET['max_date']) && $_GET['max_date'] != ''){
			$where_clause[] = ' r.order_time <= ' . strtotime($_GET['max_date']);
		}

        if(isset($_GET['tags']) && $_GET['tags'] != '') {
            $tags = '';
            foreach(explode(' ', $_GET['tags']) as $tag) {
                $tags .= '\'' . $tag . '\',';
            }
            $where_clause[] = ' tg.tag_name IN (' . rtrim($tags, ',') . ')';
        }

        if(isset($_GET['search']) && $_GET['search'] != '') {
            if(is_numeric($_GET['search'])) {
                $where_clause[] = ' r.receipt_id ' . $_GET['search'];
            }else{
                $where_clause[] = ' r.etsy_username = \'' . $_GET['search'] . '\'';
            }
        }

		if(count($where_clause) > 0){
			$where_clause = implode(' AND ', $where_clause);
			$where_clause = 'WHERE ' . $where_clause;
		}else{
			$where_clause = '';
		}

		return $where_clause;
	}
}