<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Api\Shopify;
use AppBundle\Api\Etsy;
use AppBundle\Api\Shapeways;
use GuzzleHttp\Psr7;

class ApiController extends Controller
{
    private $shopify;
    private $etsy;
    private $shapeways;

    private function getShopify()
    {
        if(is_null($this->shopify)){
            $userManager = $this->get('fos_user.user_manager');
            $user = $this->get('security.context')->getToken()->getUser();
            $this->shopify = new Shopify($this->get('logger'), $userManager, $user, $this->get('router'));
        }
        return $this->shopify;
    }

    private function getEtsy()
    {
        if(is_null($this->etsy)){
            $userManager = $this->get('fos_user.user_manager');
            $user = $this->get('security.context')->getToken()->getUser();
            $this->etsy = new Etsy($this->get('logger'), $userManager, $user);
        }
        return $this->etsy;
    }

    private function getShapeways()
    {
        if(is_null($this->shapeways)){
            $userManager = $this->get('fos_user.user_manager');
            $user = $this->get('security.context')->getToken()->getUser();
            $this->shapeways = new Shapeways($this->get('logger'), $userManager, $user);
        }
        return $this->shapeways;
    }

    /**
     * @Route("/shopify/connect", name="shopify_connect")
     */
    public function shopifyConnectAction(Request $request)
    {
        $redirect_url = $this->generateUrl('shopify_verify', [], UrlGeneratorInterface::ABSOLUTE_URL);
        return $this->getShopify()->requestRequestToken($redirect_url);
    }

    /**
     * @Route("shopify/verify", name="shopify_verify")
     */
    public function shopifyVerifyAction(Request $request)
    {
        $this->getShopify()->requestAccessToken($request);
        return $this->redirect($this->getConfigRoute());
    }

    /**
     * @Route("/etsy/connect", name="etsy_connect")
     */
    public function etsyConnectAction(Request $request)
    {
        $redirect_url = $this->generateUrl('etsy_verify', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $login_url = $this->getEtsy()->requestRequestToken($redirect_url);
        return $this->redirect($login_url);
    }

    /**
     * @Route("etsy/verify", name="etsy_verify")
     */
    public function etsyVerifyAction(Request $request)
    {
        $this->getEtsy()->requestAccessToken($request);
        return $this->redirect($this->getConfigRoute());
    }

    /**
     * @Route("/shapeways/connect", name="shapeways_connect")
     */
    public function shapewaysConnectAction(Request $request)
    {
        $redirect_url = $this->generateUrl('shapeways_verify', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $login_url = $this->getShapeways()->requestRequestToken($redirect_url);
        return $this->redirect($login_url);
    }

    /**
     * @Route("shapeways/verify", name="shapeways_verify")
     */
    public function shapewaysVerifyAction(Request $request)
    {
        $this->getShapeways()->requestAccessToken($request);
        return $this->redirect($this->getConfigRoute());
    }

    private function getConfigRoute()
    {
        return ($this->container->get('kernel')->getEnvironment() == 'dev' ? '/app_dev.php' : '') . '/config';
    }

    /**
     * @Route("etsy/user", name="etsy_user")
     */
    public function getEtsyUser(Request $request)
    {
        echo "<pre>";
        print_r($this->getEtsy()->getLoggedInUserShop());
        echo "</pre>";
        return null;
    }
}
