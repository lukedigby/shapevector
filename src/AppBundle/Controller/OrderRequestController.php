<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderRequestController extends Controller
{
	private $secondsToSleep = 3;

	private function getOrderRequest()
	{
		$this->getDoctrine()->getManager()->clear();
		return $this->getDoctrine()->getRepository('AppBundle:OrderRequest')
		->findOneBy(
			[
			'is_processing' => true,
			'is_completed'  => false,
			'user_id'       => $this->getUser()->getId()
			]
			)
		;
	}

  private function getNewOrderRequest($lastResultCount)
  {
    $orderRequest = $this->getOrderRequest();
    if($orderRequest){
      $currentResultCount = $orderRequest->getCurrentResultCount(); 
      if(is_null($currentResultCount) || $currentResultCount == $lastResultCount){

        time_sleep_until(time() + $this->secondsToSleep);
        return $this->getNewOrderRequest($lastResultCount);
      }
      return $orderRequest;
    } 
  }

  /**
   * @Route("/getProcessingOrderStatus", name="processingOrderStatus")
   */
  public function checkProcessingOrdersStatusAction(Request $request)
  {
  	$orderRequest = $this->getNewOrderRequest($request->query->get('lastResultCount'));
    if(!$orderRequest){
  		$response = [
  		'status' => 'no processing orders'
  		];
  		$this->get('session')->remove('processing_orders');
    }else{
      $response = [
      'processed_results' => $orderRequest->getCurrentResultCount(),
      'total_results' => $orderRequest->getTotalResultCount()
      ];
  	}
  	return new Response(json_encode($response));
  }
}
