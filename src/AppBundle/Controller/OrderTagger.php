<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Transaction;

class OrderTagger
{
	protected $tagRules;

	public function __construct($doctrine, $em, $user)
	{
		$this->em = $em;
    $this->tagRules = $doctrine->getRepository('AppBundle:TagRule')->findByUser($user);
	}

  public function applyTags(Transaction $transaction, $item, $variations = null)
  {
    if(!$variations){
      $variations = '';
      if(isset($item->properties)){
        foreach($item->properties as $variation){
          $variations .= $variation->name . ' ' . $variation->value . ' ';
        } 
      }
    }

    foreach($this->tagRules as $rule){
      if($rule->getField() == ''){
        $transaction->addTag($this->em->getReference('AppBundle\Entity\Tag', $rule->getTag()->getId()));
      }else{
        $t_field = '';
        if($rule->getField() == 'name') {
          $t_field = $item->title;
        } else if($rule->getField() == 'variations') {
          $t_field = $variations;
        }
        if(!!preg_match('/\b'.preg_quote($rule->getValue(), '/') . '\b/i', $t_field)) {
          $transaction->addTag($this->em->getReference('AppBundle\Entity\Tag', $rule->getTag()->getId()));
        }
      }
    }
  }

}