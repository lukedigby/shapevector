<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Api\Shopify;
use AppBundle\Entity\Receipt;
use AppBundle\Entity\Transaction;
use AppBundle\Controller\OrderTagger;

class ShopifyOrderController extends Controller
{
	protected $api;
	protected $logger;
	protected $userManager;
	protected $em;
	protected $user;
	protected $doctrine;
	protected $router;
	protected $orders;
	protected $receiptRepository;
	protected $lastOrderTime;
	protected $applyTags;

	public function __construct($logger, $userManager, $doctrine, $em, $router)
	{
		$this->logger = $logger;
		$this->userManager = $userManager;
		$this->doctrine = $doctrine;
		$this->em = $em;
		$this->router = $router;
	}

	public function setUser($user)
	{
		$this->user = $user;
		$this->setupApi();
		$this->setupOrderTagger();
	}

	private function setupApi()
	{
		$this->api = new Shopify($this->logger, $this->userManager, $this->user, $this->router);
	}

	private function setupOrderTagger()
	{
		$this->orderTagger = new OrderTagger($this->doctrine, $this->em, $this->user);
	}

	public function getNewOrderCount()
	{
		$this->receiptRepository = $this->doctrine->getRepository(Receipt::class);
    $latestOrder = $this->receiptRepository->findOneBy(['userId' => $this->user->getId(), 'source' => 'shopify'], ['id' => 'DESC']);
    $this->lastOrderTime = $latestOrder ? $latestOrder->getOrderTime() : null;
        $apiResponse = $this->api->getOrderCount($this->lastOrderTime);
		return $apiResponse->count;
	}

	public function saveNewOrders($orderRequest)
	{
    $loadedResultCount = 0;
    $page = 0;
    do{
    	/*
    	 * Attempt to fetch a page of orders and quit if none are found
    	 */
      if(!$this->fetchOrders($page)){ return false; }
      $page++;

      foreach($this->orders as $order){
      	$loadedResultCount++;
      	/*
      	 * Check if the order has already been stored in the database, and if so, skip it
      	 */
        if($this->receiptRepository->findBy(['id' => $order->id])){
          $this->logger->info('Receipt already exists with ID: ' . $order->id);
          continue;
        }

        /* 
         * Try to map the order and save it to the database
         */
        try{
          $receipt = new Receipt();
          $this->setReceiptData($order, $receipt);
          foreach($order->line_items as $item){
            $this->setTransactionData($item, $receipt);
          }
        	$this->em->persist($receipt);
        }catch(\Exception $e){
          $this->logger->error('Error processing order'.$e->getMessage());
          $this->logger->error(print_r($order, true));
        }
        
        /*
         * Update the progress of the Order Request and save it to the database
         */
        $orderRequest->setCurrentResultCount($loadedResultCount);
        $this->em->flush();
      }
    }while($orderRequest->getTotalResultCount() > $loadedResultCount);
	}

	private function fetchOrders($page)
	{
		try{
      $this->orders = $this->api->getOrders($this->lastOrderTime, $page);
      return true;
    }catch(RequestException $e){
      $this->logger->error('Error Fetching Orders');
      if($e->hasResponse()){
        $this->logger->error($e->getResponse());
      }
    }
  	return false;
	}

	private function setReceiptData($order, Receipt $receipt)
	{
		$receipt->setUser($this->user);
    $receipt->setSource('shopify');
    $receipt->setId($order->id);
    $receipt->setOrderId($order->order_number);
    $receipt->setOrderTime(strtotime($order->created_at));
    $receipt->setBuyerEmail($order->email);
    $receipt->setFirstName($order->customer->first_name);
    $receipt->setLastName($order->customer->last_name);
    if(isset($order->shipping_address)){
    $receipt->setFirstName($order->shipping_address->first_name);
    $receipt->setLastName($order->shipping_address->last_name);
      $receipt->setAddress1($order->shipping_address->address1);
      $receipt->setAddress2($order->shipping_address->address2);
      $receipt->setCity($order->shipping_address->city);
      $receipt->setState($order->shipping_address->province_code);
      $receipt->setZip($order->shipping_address->zip);
      $receipt->setCountry($order->shipping_address->country);
      $receipt->setCountryCode($order->shipping_address->country_code);
    }
	}

	private function setTransactionData($item, $receipt)
	{
		$transaction = new Transaction();
    $transaction->setId($item->id);
    $transaction->setItemName($item->title);
    $transaction->setItemQuantity($item->quantity);

    $transaction->setReceipt($receipt);
    $this->setTransactionExtraDetails($item, $transaction);

    if(isset($item->product)){
      if (preg_match('/(\d+) (\d+)\s*$/', strip_tags($item->product->body_html), $numbers)){
        $transaction->setModelId($numbers[1]);
        $transaction->setMaterialId($numbers[2]);
      }
      if(!$transaction->getImageUrl()){
        $this->logger->info('images type'.gettype($item->product->images));
        $this->logger->info('images reset type'.gettype(reset($item->product->images)));
        if(reset($item->product->images)){
          $transaction->setImageUrl(reset($item->product->images)->src);
        }
      }
    }

    $this->orderTagger->applyTags($transaction, $item);

    $this->em->persist($transaction);
	}

	private function setTransactionExtraDetails($item, $transaction)
	{
		$extraDetails = [];
    foreach($item->properties as $property){
      if(strpos($property->name, '_') === 0){ 
        continue; 
      }elseif($property->name == 'Image to Use'){ 
        $transaction->setImageUrl($property->value); 
      }elseif($property->name == 'Order Notes to Seller'){ 
        $receipt->setBuyerMessage($property->value); 
      }else{
        if($property->name == 'Email address to send Preview'){ $property->name = 'Email address'; }
        if(strpos($property->name, 'Add ') === 0){ 
          $property->value = substr($property->name, 4); 
          $property->name = 'Additions';
        }
        $extraDetails[] = $property;
      }
    }
    $transaction->setExtraDetails($extraDetails);
	}
} 