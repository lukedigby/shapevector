<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Put;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Doctrine\Common\Collections\Criteria;
use AppBundle\Api\Shapeways;
use AppBundle\Api\Shopify;
use AppBundle\Entity\Receipt;


class MainController extends FOSRestController
{
  /**
   * @Route("/", name="main")
   */
  public function indexAction()
  { 
    return $this->render('index.html.twig', array(
      'processing_orders' => $this->get('session')->has('processing_orders'),
      ));
  }

  /**
   * @Put("/updateConfig", name="update_config")
   */
  public function updateConfigAction(Request $request)
  {
    $userManager = $this->container->get('fos_user.user_manager');
    $user = $this->getUser();
    $user->setShopifyShopName($request->request->get('shopifyShopName'));
    $userManager->updateUser($user);

    $response = [
      'status' => 'success'
    ];

    $view = $this->view($response, 200)->setFormat('json');
    return $this->handleView($view);
  }

  /**
   * @Route("/updateTrackingInfo", name="updateTrackingInfo")
   */
  public function updateTrackingInfoAction(Request $request)
  {
    $shapeways = new Shapeways($this->get('logger'), $this->get('fos_user.user_manager'), $this->getUser());
    $shopify = new Shopify($this->get('logger'), $this->get('fos_user.user_manager'), $this->getUser(), $this->get('router'));
    $em = $this->getDoctrine()->getManager();
    $receiptRepository = $em->getRepository(Receipt::class);

    $criteria = Criteria::create()
      ->where(Criteria::expr()->eq('userId', $this->getUser()->getId()))
      ->andWhere(Criteria::expr()->neq('shapewaysOrderNumber', null))
      ->andWhere(Criteria::expr()->isNull('trackingNumber'));
    $receipts = $receiptRepository->matching($criteria);

    foreach($receipts as $receipt){
      $order = json_decode($shapeways->getOrder($receipt->getShapewaysOrderNumber()));
        print_r($order); 
      if($order->ordersStatus->{$receipt->getShapewaysOrderNumber()}->status == "shipped"){
        $receipt->setTrackingNumber($order->ordersInfo[0]->shipments[0]->trackingNumber);
        $receipt->setCourier($order->ordersInfo[0]->shipments[0]->carrier);
        $shopify->putTrackingDetails($receipt);
      }
    }
    $em->flush();
    return $this->redirectToRoute('main');
  }

}