<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TagRepository")
 */
class Tag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
    * @ORM\OneToMany(targetEntity="TagRule", mappedBy="tag", fetch="EXTRA_LAZY")
    */
    protected $tag_rules;

    /**
     * @ORM\ManyToMany(targetEntity="Transaction", mappedBy="tags")
     * @ORM\JoinTable(name="transaction_tag")
     */
    private $transactions;

    public function __construct() {
        $this->transactions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tag_rules = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Has transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function hasTransactions()
    {
        return !empty($this->transactions);
    }
}
