<?php 

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="usernameCanonical",
 *          column=@ORM\Column(
 *              name     = "username_canonical",
 *              length   = 191,
 *              unique   = true
 *          )
 *      ),
 *      @ORM\AttributeOverride(name="emailCanonical",
 *          column=@ORM\Column(
 *              name     = "email_canonical",
 *              length   = 191,
 *              unique   = true
 *          )
 *      )
 * })
 */
class User extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $shopify_token;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $shopify_shop_name;
  
  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  protected $shapeways_token;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  protected $shapeways_token_secret;
  
  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  protected $etsy_token;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  protected $etsy_token_secret;

  /**
   * @ORM\OneToMany(targetEntity="Receipt", mappedBy="user", fetch="EXTRA_LAZY")
   */
  protected $receipts;

  /**
   * @ORM\OneToMany(targetEntity="TagRule", mappedBy="user", fetch="EXTRA_LAZY")
   */
  protected $tag_rules;

	public function __construct()
	{
    $this->receipts = new ArrayCollection();
    $this->tag_rules = new ArrayCollection();
		parent::__construct();
	}

  /**
   * Set shopifyToken
   *
   * @param string $shopifyToken
   *
   * @return User
   */
  public function setShopifyToken($shopifyToken)
  {
    $this->shopify_token = $shopifyToken;

    return $this;
  }

  /**
   * Get shopifyToken
   *
   * @return string
   */
  public function getShopifyToken()
  {
    return $this->shopify_token;
  }

  /**
   * Set shopifyShopName
   *
   * @param string $shopifyShopName
   *
   * @return User
   */
  public function setshopifyShopName($shopifyShopName)
  {
    $this->shopify_shop_name = $shopifyShopName;

    return $this;
  }

  /**
   * Get shopifyShopName
   *
   * @return string
   */
  public function getshopifyShopName()
  {
    return $this->shopify_shop_name;
  }

  /**
   * Set shapewaysToken
   *
   * @param string $shapewaysToken
   *
   * @return User
   */
  public function setShapewaysToken($shapewaysToken)
  {
    $this->shapeways_token = $shapewaysToken;

    return $this;
  }

  /**
   * Get shapewaysToken
   *
   * @return string
   */
  public function getShapewaysToken()
  {
    return $this->shapeways_token;
  }

  /**
   * Set shapewaysTokenSecret
   *
   * @param string $shapewaysTokenSecret
   *
   * @return User
   */
  public function setShapewaysTokenSecret($shapewaysTokenSecret)
  {
    $this->shapeways_token_secret = $shapewaysTokenSecret;

    return $this;
  }

  /**
   * Get shapewaysTokenSecret
   *
   * @return string
   */
  public function getShapewaysTokenSecret()
  {
    return $this->shapeways_token_secret;
  }

  /**
   * Set etsyToken
   *
   * @param string $etsyToken
   *
   * @return User
   */
  public function setEtsyToken($etsyToken)
  {
    $this->etsy_token = $etsyToken;

    return $this;
  }

  /**
   * Get etsyToken
   *
   * @return string
   */
  public function getEtsyToken()
  {
    return $this->etsy_token;
  }

  /**
   * Set etsyTokenSecret
   *
   * @param string $etsyTokenSecret
   *
   * @return User
   */
  public function setEtsyTokenSecret($etsyTokenSecret)
  {
    $this->etsy_token_secret = $etsyTokenSecret;

    return $this;
  }

  /**
   * Get etsyTokenSecret
   *
   * @return string
   */
  public function getEtsyTokenSecret()
  {
    return $this->etsy_token_secret;
  }

  /**
   * Add receipt
   *
   * @param \AppBundle\Entity\Receipt $receipt
   *
   * @return User
   */
  public function addReceipt(\AppBundle\Entity\Receipt $receipt)
  {
    $this->receipts[] = $receipt;

    return $this;
  }

  /**
   * Remove receipt
   *
   * @param \AppBundle\Entity\Receipt $receipt
   */
  public function removeReceipt(\AppBundle\Entity\Receipt $receipt)
  {
    $this->receipts->removeElement($receipt);
  }

  /**
   * Get receipts
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getReceipts()
  {
    return $this->receipts;
  }

  /**
   * Get one page of receipts
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getReceiptsPage($page = 1, $where = null)
  {
    $criteria = Criteria::create()
      ->orderBy(array("orderTime" => Criteria::DESC))
      ->setFirstResult(($page-1)*10)
      ->setMaxResults(10);

    $result = new \stdClass;
    $result->receipts = $this->receipts->matching($criteria);
    $result->pagination = $this->getReceiptPagination($page);
    return $result;
  }

  private function getReceiptPagination($page)
  {
    $totalReceipts = $this->receipts->count();
    $limit = 10;
    $range = 5;
    $midRange = floor($range/2);
    $finalPage = ceil($totalReceipts / 10);

    if($page - $midRange < 1){
      $firstPage = 1;
    }elseif($page + $midRange > $finalPage){
      $firstPage = $finalPage - $range + 1;
    }else{
      $firstPage = $page - $midRange;
    }

    if($page + $midRange > $finalPage){
      $lastPage = $finalPage;
    }elseif($page - $midRange < 1){
      $lastPage = $range;
    }else{
      $lastPage = $page + $midRange;
    }

    $paging = new \stdClass;
    $paging->currentPage = $page;
    $paging->firstPage = $firstPage;
    $paging->lastPage = $lastPage;
    $paging->finalPage = $finalPage;
    $paging->firstResult = ($page - 1) * $limit + 1;
    $paging->lastResult = min($page * $limit, $totalReceipts);
    $paging->resultCount = $totalReceipts;
    return $paging;
  }
  
}
