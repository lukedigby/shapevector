<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="transaction")
 */
class Transaction
{
    /**
     * @ORM\Column(type="bigint")
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $modelId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $modelName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $materialId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $orderNotes;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $itemName;

    /**
     * @ORM\Column(type="integer")
     */
    private $itemQuantity;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $itemVariations;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $imageUrl;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $extraDetails;

    /**
     * @ORM\ManyToOne(targetEntity="Receipt", inversedBy="transactions")
     */
    private $receipt;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="transactions")
     */
    private $tags;

    public function __construct() {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Transaction
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set modelId
     *
     * @param integer $modelId
     *
     * @return Transaction
     */
    public function setModelId($modelId)
    {
        $this->modelId = $modelId;

        return $this;
    }

    /**
     * Get modelId
     *
     * @return integer
     */
    public function getModelId()
    {
        return $this->modelId;
    }

    /**
     * Set modelName
     *
     * @param integer $modelName
     *
     * @return Transaction
     */
    public function setModelName($modelName)
    {
        $this->modelName = $modelName;

        return $this;
    }

    /**
     * Get modelName
     *
     * @return integer
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * Set materialId
     *
     * @param integer $materialId
     *
     * @return Transaction
     */
    public function setMaterialId($materialId)
    {
        $this->materialId = $materialId;

        return $this;
    }

    /**
     * Get materialId
     *
     * @return integer
     */
    public function getMaterialId()
    {
        return $this->materialId;
    }

    /**
     * Set orderNotes
     *
     * @param string $orderNotes
     *
     * @return Transaction
     */
    public function setOrderNotes($orderNotes)
    {
        $this->orderNotes = $orderNotes;

        return $this;
    }

    /**
     * Get orderNotes
     *
     * @return string
     */
    public function getOrderNotes()
    {
        return $this->orderNotes;
    }

    /**
     * Set itemName
     *
     * @param string $itemName
     *
     * @return Transaction
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * Set itemQuantity
     *
     * @param integer $itemQuantity
     *
     * @return Transaction
     */
    public function setItemQuantity($itemQuantity)
    {
        $this->itemQuantity = $itemQuantity;

        return $this;
    }

    /**
     * Get itemQuantity
     *
     * @return integer
     */
    public function getItemQuantity()
    {
        return $this->itemQuantity;
    }

    /**
     * Set itemVariations
     *
     * @param string $itemVariations
     *
     * @return Transaction
     */
    public function setItemVariations($itemVariations)
    {
        $this->itemVariations = $itemVariations;

        return $this;
    }

    /**
     * Get itemVariations
     *
     * @return string
     */
    public function getItemVariations()
    {
        return $this->itemVariations;
    }

    /**
     * Set imageUrl
     *
     * @param string $imageUrl
     *
     * @return Transaction
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set extraDetails
     * 
     * @param array|object $extraDetails
     * 
     * @return Transaction
     */
    public function setExtraDetails($extraDetails)
    {
        $this->extraDetails = json_encode($extraDetails);

        return $this;
    }

    /**
     * Get extraDetails
     * 
     * @return array
     */
    public function getExtraDetails()
    {
        return json_decode($this->extraDetails);
    }

    /**
     * Set receipt
     *
     * @param \AppBundle\Entity\Receipt $receipt
     *
     * @return Transaction
     */
    public function setReceipt(\AppBundle\Entity\Receipt $receipt = null)
    {
        $this->receipt = $receipt;

        return $this;
    }

    /**
     * Get receipt
     *
     * @return \AppBundle\Entity\Receipt
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return Transaction
     */
    public function addTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }
}
