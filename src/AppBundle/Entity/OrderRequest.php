<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderRequest
 *
 * @ORM\Table(name="order_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRequestRepository")
 */
class OrderRequest
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default"=false})
     */
    private $is_processing;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $processing_start_time;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default"=false})
     */
    private $is_completed;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $current_result_count;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $total_result_count;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return OrderRequest
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set isProcessing
     *
     * @param  $isProcessing
     *
     * @return OrderRequest
     */
    public function setIsProcessing($isProcessing)
    {
        $this->is_processing = $isProcessing;

        return $this;
    }

    /**
     * Get isProcessing
     *
     * @return boolean
     */
    public function getIsProcessing()
    {
        return $this->is_processing;
    }

    /**
     * Set isCompleted
     *
     * @param  $isCompleted
     *
     * @return OrderRequest
     */
    public function setIsCompleted($isCompleted)
    {
        $this->is_completed = $isCompleted;

        return $this;
    }

    /**
     * Get isCompleted
     *
     * @return boolean
     */
    public function getIsCompleted()
    {
        return $this->is_completed;
    }

    /**
     * Set processingStartTime
     *
     * @param integer $processingStartTime
     *
     * @return OrderRequest
     */
    public function setProcessingStartTime($processingStartTime)
    {
        $this->processing_start_time = $processingStartTime;

        return $this;
    }

    /**
     * Get processingStartTime
     *
     * @return integer
     */
    public function getProcessingStartTime()
    {
        return $this->processing_start_time;
    }

    /**
     * Set currentResultCount
     *
     * @param integer $currentResultCount
     *
     * @return OrderRequest
     */
    public function setCurrentResultCount($currentResultCount)
    {
        $this->current_result_count = min($currentResultCount, $this->getTotalResultCount());

        return $this;
    }

    /**
     * Get currentResultCount
     *
     * @return integer
     */
    public function getCurrentResultCount()
    {
        return $this->current_result_count;
    }

    /**
     * Set totalResultCount
     *
     * @param integer $totalResultCount
     *
     * @return OrderRequest
     */
    public function setTotalResultCount($totalResultCount)
    {
        $this->total_result_count = $totalResultCount;

        return $this;
    }

    /**
     * Get totalResultCount
     *
     * @return integer
     */
    public function getTotalResultCount()
    {
        return $this->total_result_count;
    }
}
