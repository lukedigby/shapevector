<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReceiptRepository")
 * @ORM\Table(name="receipt")
 */
class Receipt
{
  /**
   * @ORM\Column(type="bigint")
   * @ORM\Id
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $source;

  /**
   * @ORM\Column(type="integer")
   */
  private $userId;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $orderId;

  /**
   * @ORM\Column(type="decimal", precision=20, scale=0)
   */
  private $orderTime;

  /**
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private $buyerEtsyUsername;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $buyerEmail;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private $buyerMessage;

  /**
   * @ORM\Column(type="string", length=50)
   */
  private $firstName;

  /**
   * @ORM\Column(type="string", length=50)
   */
  private $lastName;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $address1;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $address2;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $city;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $state;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $zip;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $country;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $countryCode;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $shapewaysOrderNumber;

  /**
   * @ORM\Column(type="date", nullable=true)
   */
  private $targetShippingDate;

  /**
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private $trackingNumber;

  /**
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private $courier;

  /**
   * @ORM\OneToMany(targetEntity="Transaction", mappedBy="receipt")
   */
  private $transactions;

  /**
   * @ORM\ManyToOne(targetEntity="User", inversedBy="receipts")
   * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
   */
  private $user;

  public function __construct()
  {
		$this->transactions = new ArrayCollection();
	}

  /**
   * Set id
   *
   * @param integer $id
   *
   * @return Receipt
   */
  public function setId($id)
  {
    $this->id = $id;

    return $this;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set source
   *
   * @param integer $source
   *
   * @return Receipt
   */
  public function setSource($source)
  {
    $this->source = $source;

    return $this;
  }

  /**
   * Get source
   *
   * @return integer
   */
  public function getSource()
  {
    return $this->source;
  }

  /**
   * Set orderId
   *
   * @param integer $orderId
   *
   * @return Receipt
   */
  public function setOrderId($orderId)
  {
  	$this->orderId = $orderId;

  	return $this;
  }

  /**
   * Get orderId
   *
   * @return integer
   */
  public function getOrderId()
  {
  	return $this->orderId;
  }

  /**
   * Set buyerEtsyUsername
   *
   * @param string $buyerEtsyUsername
   *
   * @return Receipt
   */
  public function setBuyerEtsyUsername($buyerEtsyUsername)
  {
  	$this->buyerEtsyUsername = $buyerEtsyUsername;

  	return $this;
  }

  /**
   * Get buyerEtsyUsername
   *
   * @return string
   */
  public function getBuyerEtsyUsername()
  {
  	return $this->buyerEtsyUsername;
  }

  /**
   * Set buyerEmail
   *
   * @param string $buyerEmail
   *
   * @return Receipt
   */
  public function setBuyerEmail($buyerEmail)
  {
  	$this->buyerEmail = $buyerEmail;

  	return $this;
  }

  /**
   * Get buyerEmail
   *
   * @return string
   */
  public function getBuyerEmail()
  {
  	return $this->buyerEmail;
  }

  /**
   * Set firstName
   *
   * @param string $firstName
   *
   * @return Receipt
   */
  public function setFirstName($firstName)
  {
  	$this->firstName = $firstName;

  	return $this;
  }

  /**
   * Get firstName
   *
   * @return string
   */
  public function getFirstName()
  {
  	return $this->firstName;
  }

  /**
   * Set lastName
   *
   * @param string $lastName
   *
   * @return Receipt
   */
  public function setLastName($lastName)
  {
  	$this->lastName = $lastName;

  	return $this;
  }

  /**
   * Get lastName
   *
   * @return string
   */
  public function getLastName()
  {
  	return $this->lastName;
  }

  /**
   * Set address1
   *
   * @param string $address1
   *
   * @return Receipt
   */
  public function setAddress1($address1)
  {
  	$this->address1 = $address1;

  	return $this;
  }

  /**
   * Get address1
   *
   * @return string
   */
  public function getAddress1()
  {
  	return $this->address1;
  }

  /**
   * Set address2
   *
   * @param string $address2
   *
   * @return Receipt
   */
  public function setAddress2($address2)
  {
  	$this->address2 = $address2;

  	return $this;
  }

  /**
   * Get address2
   *
   * @return string
   */
  public function getAddress2()
  {
  	return $this->address2;
  }

  /**
   * Set city
   *
   * @param string $city
   *
   * @return Receipt
   */
  public function setCity($city)
  {
  	$this->city = $city;

  	return $this;
  }

  /**
   * Get city
   *
   * @return string
   */
  public function getCity()
  {
  	return $this->city;
  }

  /**
   * Set state
   *
   * @param string $state
   *
   * @return Receipt
   */
  public function setState($state)
  {
  	$this->state = $state;

  	return $this;
  }

  /**
   * Get state
   *
   * @return string
   */
  public function getState()
  {
  	return $this->state;
  }

  /**
   * Set zip
   *
   * @param string $zip
   *
   * @return Receipt
   */
  public function setZip($zip)
  {
  	$this->zip = $zip;

  	return $this;
  }

  /**
   * Get zip
   *
   * @return string
   */
  public function getZip()
  {
  	return $this->zip;
  }

  /**
   * Set country
   *
   * @param string $country
   *
   * @return Receipt
   */
  public function setCountry($country)
  {
  	$this->country = $country;

  	return $this;
  }

  /**
   * Get country
   *
   * @return string
   */
  public function getCountry()
  {
  	return $this->country;
  }

  /**
   * Set countryCode
   *
   * @param string $countryCode
   *
   * @return Receipt
   */
  public function setCountryCode($countryCode)
  {
  	$this->countryCode = $countryCode;

  	return $this;
  }

  /**
   * Get countryCode
   *
   * @return string
   */
  public function getCountryCode()
  {
  	return $this->countryCode;
  }

  /**
   * Set shapewaysOrderNumber
   *
   * @param integer $shapewaysOrderNumber
   *
   * @return Receipt
   */
  public function setShapewaysOrderNumber($shapewaysOrderNumber)
  {
  	$this->shapewaysOrderNumber = $shapewaysOrderNumber;

  	return $this;
  }

  /**
   * Get shapewaysOrderNumber
   *
   * @return integer
   */
  public function getShapewaysOrderNumber()
  {
  	return $this->shapewaysOrderNumber;
  }

  /**
   * Set targetShippingDate
   *
   * @param \DateTime $targetShippingDate
   *
   * @return Receipt
   */
  public function setTargetShippingDate($targetShippingDate)
  {
  	$this->targetShippingDate = $targetShippingDate;

  	return $this;
  }

  /**
   * Get targetShippingDate
   *
   * @return \DateTime
   */
  public function getTargetShippingDate()
  {
  	return $this->targetShippingDate;
  }

  /**
   * Set trackingNumber
   *
   * @param string $trackingNumber
   *
   * @return Receipt
   */
  public function setTrackingNumber($trackingNumber)
  {
  	$this->trackingNumber = $trackingNumber;

  	return $this;
  }

  /**
   * Get trackingNumber
   *
   * @return string
   */
  public function getTrackingNumber()
  {
  	return $this->trackingNumber;
  }

  /**
   * Set courier
   *
   * @param string $courier
   *
   * @return Receipt
   */
  public function setCourier($courier)
  {
  	$this->courier = $courier;

  	return $this;
  }

  /**
   * Get courier
   *
   * @return string
   */
  public function getCourier()
  {
  	return $this->courier;
  }

    /**
     * Add transaction
     *
     * @param \AppBundle\Entity\Transaction $transaction
     *
     * @return Receipt
     */
    public function addTransaction(\AppBundle\Entity\Transaction $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \AppBundle\Entity\Transaction $transaction
     */
    public function removeTransaction(\AppBundle\Entity\Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Set orderTime
     *
     * @param string $orderTime
     *
     * @return Receipt
     */
    public function setOrderTime($orderTime)
    {
        $this->orderTime = $orderTime;

        return $this;
    }

    /**
     * Get orderTime
     *
     * @return string
     */
    public function getOrderTime()
    {
        return $this->orderTime;
    }

    /**
     * Set buyerMessage
     *
     * @param string $buyerMessage
     *
     * @return Receipt
     */
    public function setBuyerMessage($buyerMessage)
    {
        $this->buyerMessage = $buyerMessage;

        return $this;
    }

    /**
     * Get buyerMessage
     *
     * @return string
     */
    public function getBuyerMessage()
    {
        return $this->buyerMessage;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Receipt
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Receipt
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
