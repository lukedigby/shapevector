<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
	/**
	* @var ContainerInterface
	*/
  private $container;

  public function setContainer(ContainerInterface $container = null)
  {
      $this->container = $container;
  }

	public function load(ObjectManager $manager)
	{
		$userManager = $this->container->get('fos_user.user_manager');

		$userAdmin = $userManager->createUser();
		$userAdmin->setUsername('admin');
		$userAdmin->setPlainPassword('123456');
		$userAdmin->setEmail('test@domain.com');
		$userAdmin->setEnabled(true);
		$userAdmin->setRoles(['ROLE_ADMIN']);

		$userManager->updateUSer($userAdmin, true);
	}
}