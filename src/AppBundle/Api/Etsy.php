<?php
namespace AppBundle\Api;

use Symfony\Component\HttpFoundation\Request;

class Etsy extends BaseApi
{
	private $scopes = [
		'email_r', 
		'listings_r', 
		'transactions_r'
	];

	protected $name = 'Etsy';
  protected $baseUri = 'https://openapi.etsy.com/v2/';
	protected $requestPath = 'oauth/request_token';
	protected $accessPath = 'oauth/access_token';

	protected $consumerKey = 'l2s447xn16mn0fif1wyv59jw';
	protected $consumerSecret = 'wmbf7z85d6';

	public function __construct($container, $user_manager, $user)
	{
		parent::__construct($container, $user_manager, $user);		

		$scopes = urlencode(implode(' ', $this->scopes));
		$this->requestPath .= "?scope=$scopes";
	}

	public function requestRequestToken($redirect_url)
	{
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token_secret' => '',
			'callback' => $redirect_url
		]);
		
		$res = $this->client->post($this->requestPath);
		parse_str($res->getBody(), $request_token_info);

		$this->saveTokenSecret($request_token_info['oauth_token_secret']);
		$this->userManager->updateUser($this->user);

		return $request_token_info['login_url'];
	}

	public function requestAccessToken(Request $request)
	{
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token' => $request->query->get('oauth_token'),
			'token_secret' => $this->tokenSecret,
			'verifier' => $request->query->get('oauth_verifier'),
		]);
		
		$res = $this->client->post($this->accessPath);

		parse_str($res->getBody(), $access_token_info);

		$this->saveToken($access_token_info['oauth_token']);
		$this->saveTokenSecret($access_token_info['oauth_token_secret']);
		$this->user->setEtsyUsername($this->getLoggedInUser()->login_name);
		$this->userManager->updateUser($this->user);

		return true;
	}

	public function getLoggedInUserShop()
	{
		$this->setupClientForQuery();
		try{
			$response = $this->client->get('users/Labnoesis/shops');
		}catch(Exception $e){
			$logger->error('Request: '.Psr7\str($e->getRequest()));
			$logger->error('Response: '.Psr7\str($e->getResponse()));
		}
		return json_decode($response->getBody());
	}

	public function getLatestOrders($min_created, $offset)
	{
		$this->setupClient();
    $lastUpdateTime = $min_created ? "&min_created=$min_created" : '';
    $offset = "&offset=$offset";
		$includes = '&includes=Country,Buyer,Transactions,Transactions/MainImage,Transactions/Listing,Transactions/Listing/Images:1:0,Transactions/Listing/ShippingTemplate';
		try{
			$response = $this->client->get('orders.json?'.http_build_query($query_params), ['headers' => ['X-Shopify-Access-Token' => $this->token]]);
    }catch(RequestException $e){
      $response = null;
      if($e->hasResponse()){
        $response = Psr7\str($e->getResponse());
      }
      $this->logger->error('Error Fetching Orders '. print_r($response, true));
      $this->logger->error('Page: '.$page);
      return false;
    }
		$body = json_decode($response->getBody());
		$response = $this->client->get('shops/niquegeek/receipts?limit=50'.$includes.$offset.$lastUpdateTime);
		$body = json_decode($response->getBody());
		return $body;
	}
}