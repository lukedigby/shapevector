<?php

namespace AppBundle\Api;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class Shopify extends BaseApi
{
	protected $name = 'Shopify';
  protected $baseUri = 'https://{shop}.myshopify.com/admin/';
	protected $requestPath = 'oauth/authorize';
	protected $accessPath = 'oauth/access_token';

	protected $consumerKey = '112b5e3e5407ba353284cbb138fd977f';
	protected $consumerSecret = 'e54e9e3f055520794b184fa75aea2a5c';

	public function __construct($logger, $user_manager, $user, $router)
	{
		parent::__construct($logger, $user_manager, $user);

		$this->router = $router;
		$this->baseUri = str_replace('{shop}', $this->shopName, $this->baseUri);
		$api_key = $this->consumerKey;
		$scopes = 'read_products,read_orders,write_orders';
		$redirect_url  = $this->router->generate('shopify_verify', [], UrlGeneratorInterface::ABSOLUTE_URL);
		$nonce = '54frt';
		$this->requestPath .= "?client_id=$api_key&scope=$scopes&redirect_uri=$redirect_url&state=$nonce";
	}

	protected function addKeyTokens()
	{
		$this->shopName = $this->user->getShopifyShopName();
		$this->token = $this->user->{'get' . $this->name . 'Token'}();
	}

	public function requestRequestToken($redirect_url)
	{
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token_secret' => '',
			'callback' => $redirect_url
		]);
		return new RedirectResponse($this->baseUri . $this->requestPath);
	}

	public function requestAccessToken(Request $request)
	{
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token_secret' => '',
		]);

		$key = $this->consumerKey;
		$secret = $this->consumerSecret;
		$code = $request->query->get('code');

		$this->accessPath .= "?client_id=$key&client_secret=$secret&code=$code";
		$res = $this->client->post($this->accessPath);
		$access_token_info = json_decode($res->getBody());

		$this->saveToken($access_token_info->access_token);
		$this->userManager->updateUser($this->user);

		return true;
	}

	public function getOrderCount($created_at_min = null)
	{
		$this->setupClient();
		$query_params = [];
		if($created_at_min){ $query_params['created_at_min'] = date('c', $created_at_min+1); }
		try{
		    $response = $this->client->get('orders/count.json?'.http_build_query($query_params), ['headers' => ['X-Shopify-Access-Token' => $this->token]]);
        }catch(RequestException $e){
            $response = null;
            if($e->hasResponse()){
                $response = Psr7\str($e->getResponse());
            }
            $this->logger->error('Error Fetching Order count'. print_r($response, true));
            return false;
        }catch(\Exception $e){
		    echo $e->getMessage();
            $this->logger->error('Error Fetching Order count'. print_r($e->getMessage(), true));
            return false;
        }
		$body = json_decode($response->getBody());
		return $body;
	}

	public function getOrders($created_at_min = null, $page = null)
	{
		$this->setupClient();
		$query_params = [
			'limit' => 25,
			'status' => 'any'
		];
		if($created_at_min){ $query_params['created_at_min'] = date('c', $created_at_min+1); }
		if($page){ $query_params['page'] = $page; }
  
    try{
			$response = $this->client->get('orders.json?'.http_build_query($query_params), ['headers' => ['X-Shopify-Access-Token' => $this->token]]);
    }catch(RequestException $e){
      $response = null;
      if($e->hasResponse()){
        $response = Psr7\str($e->getResponse());
      }
      $this->logger->error('Error Fetching Orders '. print_r($response, true));
      $this->logger->error('Page: '.$page);
      return false;
    }
		$body = json_decode($response->getBody());
		$orders = $body->orders;

		$transactions_by_product = [];
		//$this->logger->info('Orders', [$orders]);
		foreach($orders as $order){
			foreach($order->line_items as $item){
				if(isset($item->product_id)){
					$transactions_by_product[(string)$item->product_id][] = $item;
				}
			}
		}
		
		$products = $this->getProducts(array_keys($transactions_by_product));

		foreach($products as $product){
			foreach($transactions_by_product[(string)$product->id] as $transaction){
				$transaction->product = $product;
			}
		}
		return $orders;
	}

	public function getProducts($product_ids)
	{
		if(count($product_ids) < 1){ return []; }
		try{
			$response = $this->client->get("products.json?ids=".implode(',', $product_ids), ['headers' => ['X-Shopify-Access-Token' => $this->token]]);
    }catch(RequestException $e){
      $response = null;
      if($e->hasResponse()){
        $response = Psr7\str($e->getResponse());
      }
      $this->logger->error('Error Fetching Products', [$response]);
      $this->logger->error('Request: ', [Psr7\str($e->getRequest())]);
      $this->logger->error('IDs: '.$product_ids);
      return false;
    }
		$products =  json_decode($response->getBody())->products;
		return $products;
	}

	public function getProductImages(Array $product_ids)
	{
		$this->setupClient();
		$response = $this->client->get("products.json?fields=id,images&ids=".implode(',', $product_ids), ['headers' => ['X-Shopify-Access-Token' => $this->token]]);
		$products =  json_decode($response->getBody())->products;
		return $products;
	}

	public function putTrackingDetails($receipt)
	{
		$this->setupClient();
		$data = [
			"fulfillment" => [
				"tracking_number" => $receipt->getTrackingNumber(),
				"tracking_company" => $receipt->getCourier()
			]
		];
		$response = $this->client->post("orders/{$receipt->getOrderId()}/fulfillments.json", ['json' => $data], ['headers' => ['X-Shopify-Access-Token' => $this->token]]);
		$this->get('logger')->info('updated tracking', $response->getBody());
	}
}