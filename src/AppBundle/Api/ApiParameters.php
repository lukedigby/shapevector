<?php
namespace AppBundle\Api;

class ApiParameters
{
	public $consumer_key;
  public $consumer_secret;
  public $base_uri;
  public $request_path;
  public $access_path;

  public function __construct(Array $params)
  {
  	foreach ($params as $key => $value) {
  		if(property_exists($this, $key)){
  			$this->{$key} = $value;
  		}
  	}
  }
}