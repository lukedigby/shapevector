<?php 
namespace AppBundle\Api;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

abstract class BaseApi
{

	protected $name;
	protected $client;
	
	protected $user;
	protected $userManager;

	protected $baseUri;
	protected $requestPath;
	protected $accesPath;
	
	protected $consumerKey;
	protected $consumerSecret;
	protected $token;
	protected $tokenSecret;

	public function __construct($logger, $user_manager, $user)
	{
		$this->logger = $logger;
		$this->user = $user;
		$this->userManager = $user_manager;
		$this->addKeyTokens();
	}

	protected function setupClient($oauth_params = null){
		$clientParams = [
			'base_uri' => $this->baseUri,
			'verify' => true,
		];

		if($oauth_params){
			$stack = HandlerStack::create();
			$oauth = new Oauth1($oauth_params);
			$stack->push($oauth);
			$clientParams['auth'] = 'oauth';
			$clientParams['handler'] = $stack;
		}

		$this->client = new Client($clientParams);		
	}

	protected function setupClientForQuery(){
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token' => $this->token,
			'token_secret' => $this->tokenSecret,
		]);
		
	}
	
	protected function saveToken($token)
	{
		$this->user->{'set' . $this->name . 'Token'}($token);
	}

	protected function saveTokenSecret($token_secret){
		$this->user->{'set' . $this->name . 'TokenSecret'}($token_secret);
	}

	protected function addKeyTokens()
	{
		$this->token = $this->user->{'get' . $this->name . 'Token'}();
		$this->tokenSecret = $this->user->{'get' . $this->name . 'TokenSecret'}();
	}

	abstract public function requestRequestToken($redirect_url);

	abstract public function requestAccessToken(Request $request);
	
}