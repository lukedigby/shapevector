<?php

namespace AppBundle\Api;

use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

class Shapeways extends BaseApi
{
	protected $name = 'Shapeways';

  protected $baseUri = 'http://api.shapeways.com/';
	protected $requestPath = 'oauth1/request_token/v1';
	protected $accessPath = 'oauth1/access_token/v1';
	
	protected $consumerKey = '494eb5772ada8c22769331ec4d6674ccf2cc1616';
	protected $consumerSecret = '3c315549baa31e17c59e05c487cb8bcfd9b73b73';

	public function __construct($logger, $user_manager, $user)
	{
		parent::__construct($logger, $user_manager, $user);
	}

	protected function setupClientForQuery()
	{
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token' => $this->token,
			'token_secret' => $this->tokenSecret,
		]);
	}

	public function requestRequestToken($redirect_url)
	{
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token_secret' => '',
			'callback' => $redirect_url
		]);
		$res = $this->client->post($this->requestPath);
		parse_str($res->getBody(), $request_token_info);

		//$this->saveToken($request_token_info['oauth_token']);
		$this->saveTokenSecret($request_token_info['oauth_token_secret']);
		$this->userManager->updateUser($this->user);

		return $request_token_info['authentication_url'];
	}

	public function requestAccessToken(Request $request)
	{
		$this->setupClient([
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'token' => $request->query->get('oauth_token'),
			'token_secret' => $this->tokenSecret,
			'verifier' => $request->query->get('oauth_verifier'),
		]);

		$res = $this->client->post($this->accessPath);

		parse_str($res->getBody(), $access_token_info);

		$this->saveToken($access_token_info['oauth_token']);
		$this->saveTokenSecret($access_token_info['oauth_token_secret']);
		$this->userManager->updateUser($this->user);

		return true;
	}

	public function uploadModel($file, $scale, $logger)
	{
		$this->setupClientForQuery();
    $original_name = $file->getClientOriginalName();
    $filename = $file->getPath() . '/' . $file->getFilename();
		$file = file_get_contents($filename);
    $data = array("fileName" => $original_name,
                  "file" => rawurlencode(base64_encode($file)),
                  "hasRightsToModel" => 1,
                  "acceptTermsAndConditions" => 1,
                  "uploadScale" => $scale
                  );
		
		try{
    	$response = $this->client->post('/models/v1', ['json' => $data]);
		}catch(ClientException $e){
			$logger->error('Request: '.Psr7\str($e->getRequest()));
			$logger->error('Response: '.Psr7\str($e->getResponse()));
		}    
  	return json_decode($response->getBody());
	}

	public function getOrder($id = null)
	{
		$this->setupClientForQuery();
		if(is_null($id)){
			$id = $_GET['orderId'];
		}
		try{
			$response = $this->client->get("/orders/$id/v1/");
			return $response->getBody()->getContents();
		}catch(ClientException $e){
			if(json_decode($e->getResponse()->getBody()->getContents())->reason == printf('Invalid order filter sent to GET for order [%d]', $id)){
				return false;
			}
		}
	}

	public function postOrder($request, $logger)
	{
		$this->setupClientForQuery();
		$data = array(
			"firstName" => $request->request->get('firstName'),
			"lastName" => mb_strimwidth($request->request->get('lastName'), 0, 19),
			"country" => $request->request->get('country'),
			"state" => $request->request->get('state'),
			"city" => $request->request->get('city'),
			"address1" => $request->request->get('address1'),
			"address2" => $request->request->get('address2'),
			"zipCode" => str_replace(' ', '', $request->request->get('zipCode')),
			"phoneNumber" => "2084620462",
			"items" => $request->request->get('items'),
			"paymentVerificationId" => "SV-ORDER",
			"paymentMethod" => "credit_card",
			"shippingOption" => "Cheapest",
		);
		if($data['state'] == ''){
			unset($data['state']);
		}

		try{
   		$response = $this->client->post('/orders/v1', ['json'=>$data]);
		}catch(ClientException $e){
			$logger->error(Psr7\str($e->getRequest()));
			$logger->error(Psr7\str($e->getResponse()));
			exit;
		}
		$body = json_decode($response->getBody());
    $order = json_decode($this->getOrder($body->orderId));
    $targetShippingDate = $order->ordersInfo[0]->targetShipDate;
  	return array(
  		'orderId' => $body->orderId,
  		'targetShippingDate' => $targetShippingDate
  	);
	}

	public function cancelOrder($id)
	{
		$this->setupClientForQuery();
		$data = array(
			"orderId" => (int)$id,
			"status" => "cancelled",
		);
		try{
	    $response = $this->client->put('/orders/'.$id.'/v1', ['json' => $data]);
	    $body = json_decode($response->getBody(), true);
	    return $body;
		}catch(ClientException $e){
			if(json_decode($e->getResponse()->getBody()->getContents())->reason == 'Invalid orderId'){
				$response = [];
				$response['status'] = 'failed';
				if($this->getOrder($id)){
					$response['reason'] = 'Order is no longer able to be cancelled';
				}else{
					$response['reason'] = 'Order does not exist';
				}
				return $response;
			}
		}
	}

	public function getOrders($logger)
	{
		$this->setupClientForQuery();

		try{
			$response = $this->client->get("/orders/v1/");
		}catch(ClientException $e){
			$logger->error('Request: '.Psr7\str($e->getRequest()));
			$logger->error('Response: '.Psr7\str($e->getResponse()));
		}

		return $response->getBody();
	}

	public function getOrderTrackingInfo($id = null)
	{
		if(is_null($id)){
			$id = $_GET['orderId'];
		}
		$response = $this->getOrder($id);
		return $response->ordersInfo[0]->shipments[$id];
	}

	public function getShippingStatus($order_ids)
	{
		$shippingInfo = array();
		foreach($order_ids as $id) {
			$response = json_decode($this->getOrder($id));
			$orderInfo = $response->ordersInfo[0];
			if(count($orderInfo->shipments) > 0) {
				$shippingInfo[$orderInfo->orderId] = (array)$orderInfo->shipments[0];
			}
		}
		return $shippingInfo;
	}

	public function getMaterials($logger)
	{
		$this->setupClientForQuery();

		try{
			$response = $this->client->get("/materials/v1/");
		    return $response->getBody();
		}catch(ClientException $e){
			$logger->error('Request: '.Psr7\str($e->getRequest()));
			$logger->error('Response: '.Psr7\str($e->getResponse()));
			$logger->error('Message: '.Psr7\str($e->getMessage()));
		}catch(\Exception $e){
			$logger->error('Message: '.Psr7\str($e->getMessage()));
        }
        return [];
	}
	
}