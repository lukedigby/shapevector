<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Exception\RequestException;

use AppBundle\Api\Shopify;
use AppBundle\Entity\Receipt;
use AppBundle\Entity\OrderRequest;
use AppBundle\Entity\Transaction;

class UpdateOrdersCommand extends ContainerAwareCommand
{
  private $doctrine;
  private $em;
  private $running;
  private $userManager;
  private $etsyParams;
  private $currentOrder;
  private $logger;
  private $errors = [];   

	protected function configure()
	{
    $this->setName('update_orders');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $this->running = true;
    $this->logger = $this->getContainer()->get('logger');

    $this->userManager = $this->getContainer()->get('fos_user.userManager');
    $this->doctrine = $this->getContainer()->get('doctrine');
    $this->em = $this->doctrine->getManager();
    $users = $this->userManager->findUsers();
    foreach($users as $user){
      $this->updateOrders($user);
    }
  }
  private function stopCommand()
  {
    $this->running = false;
  }

  private function updateOrders($user)
  {
    $shopify = new Shopify($this->getContainer(), $this->userManager, $user);
    $transactionRepository = $this->doctrine->getRepository(Transaction::class);
    $receiptRepository = $this->doctrine->getRepository(Receipt::class);
    $page = 0;
    $loadedResultCount = 0;
    $totalResultCount = $shopify->getOrderCount()->count;
    do{
      $orders = $shopify->getOrders(null, $page);
      $page++;

      foreach($orders as $order){
        $loadedResultCount++;
        $receipt = $receiptRepository->findOneBy(['id' => $order->id]);
        if(!$receipt){ continue; }
        $receipt->setOrderId($order->order_number);

        foreach($order->line_items as $item){
          $transaction = $transactionRepository->findOneBy(['id' => $item->id]);
          if(!$transaction){ continue; }

          $extraDetails = [];
          foreach($item->properties as $property){
            if(strpos($property->name, '_') === 0){ 
              continue; 
            }elseif($property->name == 'Image to Use'){ 
              $transaction->setImageUrl($property->value); 
            }elseif($property->name == 'Order Notes to Seller'){ 
              $receipt->setBuyerMessage($property->value); 
            }else{
              if($property->name == 'Email address to send Preview'){ $property->name = 'Email address'; }
              if(strpos($property->name, 'Add ') === 0){ 
                $property->value = substr($property->name, 4); 
                $property->name = 'Additions';
              }
              $extraDetails[] = $property;
            }
          }
          $transaction->setExtraDetails($extraDetails);

          if(isset($item->product)){
            if (preg_match('/(\d+) (\d+)\s*$/', strip_tags($item->product->body_html), $numbers)){
              $transaction->setModelId($numbers[1]);
              $transaction->setMaterialId($numbers[2]);
            }
            if(!$transaction->getImageUrl()){
              if(isset($item->product->images) && count($item->product->images) > 0){
                $transaction->setImageUrl(reset($item->product->images)->src);
              }
            }
          }
        }
      }
      $this->em->flush();
    }while($totalResultCount > $loadedResultCount);
  }
}