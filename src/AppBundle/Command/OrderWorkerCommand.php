<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Exception\RequestException;

use AppBundle\Api\Shopify;
use AppBundle\Api\Etsy;
use AppBundle\Entity\Receipt;
use AppBundle\Entity\OrderRequest;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\TagRule;

class OrderWorkerCommand extends ContainerAwareCommand
{
  private $doctrine;
  private $em;
  private $running;
  private $userManager;
  private $etsyParams;
  private $orderRequest;
  private $logger;
  private $errors = [];

  private $shopifyOrders;

	protected function configure()
	{
    $this->setName('worker');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    if(!$this->setup($output)){ return false; }

    /*
     * Now we're ready to process the orders
     */
    $output->writeln('Getting Orders.');
    $this->shopifyOrders->saveNewOrders($this->orderRequest);

    // This will be implemented later
    //$this->loadEtsyOrders($user);

    $this->terminate();
  }

  protected function setup($output)
  {
    $this->running = true;
    $output->writeln('Running.');
    /*
     * Get Services
     */
    $this->logger = $this->getContainer()->get('logger');
    $output->writeln('Running 2.');
    $this->userManager = $this->getContainer()->get('fos_user.user_manager');
    $output->writeln('Running 3.');
    $this->doctrine = $this->getContainer()->get('doctrine');
    $output->writeln('Running 4.');
    $this->em = $this->doctrine->getManager();
    $output->writeln('Running 5.');
    $this->shopifyOrders = $this->getContainer()->get('app.shopify_orders');
    $output->writeln('Running 6.');

    /* 
     * Check if there is an Order Request to process
     */
    $output->writeln('Checking for Order Request');
    $doctrine = $this->doctrine;
    $output->writeln('check 1');
    $rep = $doctrine->getRepository(OrderRequest::class);
    $output->writeln('check 2');
    $this->orderRequest = $rep->findOneBy(['is_completed' => false],['id' => 'ASC']);
    $output->writeln('Got order Request');
    if(!$this->orderRequest){
        $output->writeln('Order Request not found');
        return false;
    }
    $output->writeln('Order Request found');

    /* 
     * Setup the Order Manager and Tag Rules for this order's user
     */
    $output->writeln('Getting user');
    $user = $this->userManager->findUserBy(array('id' => $this->orderRequest->getUserId()));
    $this->shopifyOrders->setUser($user);
    $output->writeln('Got user');

    /*
     * Update Database with total results and show that the Order Request is processing.
     */
    $newOrderCount = $this->shopifyOrders->getNewOrderCount();
    $output->writeln('Got New Order Count');
    $this->orderRequest
      ->setTotalResultCount($newOrderCount)
      ->setProcessingStartTime(time())
      ->setIsProcessing(true);
    $this->em->flush();

    return true;
  }

  private function terminate()
  {
    $this->orderRequest
      ->setIsProcessing(false)
      ->setIsCompleted(true);
    $this->em->flush();
    $this->orderRequest = null;
  }

  private function loadEtsyOrders($user)
  {
    $etsy = new Etsy($this->getContainer(), $this->userManager, $user);
    $receiptRepository = $this->doctrine->getRepository(Receipt::class);
    $latestOrder = $receiptRepository->findOneBy(['userId' => $user->getId(), 'source' => 'etsy'], ['id' => 'DESC']);
    //temp to load all orders
    //$created_at_min = $latestOrder ? $latestOrder->getOrderTime() : null;
    $created_at_min = null;
    $loadedResultCount = 0;
    $this->currentOrder
      ->setProcessingStartTime(time())
      ->setIsProcessing(true);
    $this->em->flush();
    $page = 0;
    $this->currentOrder->setTotalResultCount($etsy->getOrderCount($created_at_min)->count);
    do{
      try{
        $orders = $etsy->getOrders($created_at_min, $page);
      }catch(RequestException $e){
        $this->logger->error('Error Fetching Orders');
        if($e->hasResponse()){
          $this->logger->error($e->getResponse());
        }
        return false;
      }
      $page++;

      foreach($orders as $order){
      $loadedResultCount++;
        if($receiptRepository->findBy(['id' => $order->id])){
          $this->logger->info('Found receipt with ID: ' . $order->id);
          continue;
        }

        try{
          $receipt = new Receipt();
          $receipt->setUser($user);
          $receipt->setSource('etsy');
          $receipt->setId($order->receipt_id);  
          //$receipt->setOrderId($order->order_number);
          $receipt->setOrderTime(strtotime($order->creation_tsz));
          //$receipt->setBuyerEmail($order->email);
          $name = explode(' ', $order->name);
          $receipt->setFirstName(array_shift($name));
          $receipt->setLastName(implode($name, ' '));
          $receipt->setAddress1($order->first_line);
          $receipt->setAddress2($order->second_line);
          $receipt->setCity($order->city);
          $receipt->setState($order->state);
          $receipt->setZip($order->zip);
          $receipt->setCountry($order->Country->name);
          $receipt->setCountryCode($order->Country->iso_country_code);

          foreach($order->line_items as $item){
            $transaction = new Transaction();
            $transaction->setId($item->transaction_id);
            $transaction->setReceipt($receipt);

            $transaction->setItemName($item->title);
            $transaction->setItemQuantity($item->quantity);

            $variations = '';
            foreach($item->variations as $variation){
              $variations .= $variation->formatted_value.' ';
            } 
            $transaction->setItemVariations($variations);

            if (preg_match('/(\d+) (\d+)\s*$/', strip_tags($item->description), $numbers)){
              $transaction->setModelId($numbers[1]);
              $transaction->setMaterialId($numbers[2]);
            }
            
            if(!$transaction->getImageUrl()){
              $this->logger->info('images type '.gettype($item->product->images));
              $this->logger->info('images count '.count($item->product->images));
              $first_image = reset($item->product->images);
              $this->logger->info('images reset type '.gettype($first_image));
              if($first_image){
                $transaction->setImageUrl($first_image->src);
              }
            }
          }

          $image = !is_null($transaction->MainImage) ? $transaction->MainImage : $transaction->Listing->Images[0];
          $transaction->setImageUrl($image->url_fullxfull);

          $this->applyTags($transaction, $item, $variations);

          $this->em->persist($transaction);
        }catch(\Exception $e){
          $this->logger->error('Error processing order'.print_r($order, true));
        }
        $this->em->persist($receipt);
        $this->currentOrder
          ->setCurrentResultCount($loadedResultCount);
        $this->em->flush();
      }

      $this->em->flush();
    }while($this->currentOrder->getTotalResultCount() > $loadedResultCount);
  }

  private function stopCommand()
  {
    $this->running = false;
  }
} 