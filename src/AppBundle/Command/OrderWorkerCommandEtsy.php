<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Api\Etsy;
use AppBundle\Entity\Receipt;
use AppBundle\Entity\OrderRequest;
use AppBundle\Entity\Transaction;

class OrderWorkerCommand extends ContainerAwareCommand
{
  private $doctrine;
  private $em;
  private $running;
  private $userManager;
  private $etsyParams;
  private $currentOrder;
  private $logger;

	protected function configure()
	{
    $this->setName('worker');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $this->running = true;
    $this->logger = $this->getContainer()->get('logger');

    $this->etsyParams = $this->getContainer()->getParameter('api.etsy');
    $this->userManager = $this->getContainer()->get('fos_user.userManager');
    $this->doctrine = $this->getContainer()->get('doctrine');
    $this->em = $this->doctrine->getEntityManager();

    $this->currentOrder = $this->doctrine->getRepository(OrderRequest::class)->findOneBy(
      [
        'is_completed' => false
      ],
      ['id' => 'ASC'], 
      1
    );
    if($this->currentOrder){
      $user = $this->userManager->findUserBy(array('id' => $this->currentOrder->getUserId()));
      $this->loadOrdersAction($user);
      $this->currentOrder
        ->setIsProcessing(false)
        ->setIsCompleted(true);
      $this->currentOrder = null;
      $this->em->flush();
    }
  }

  private function stopCommand()
  {
    $this->running = false;
  }

  private function loadOrdersAction($user)
  {
    $etsy = new Etsy($this->userManager, $user, $this->etsyParams);
    $receiptRepository = $this->doctrine->getRepository(Receipt::class);
    $latestOrder = $receiptRepository->findOneBy([], ['orderTime' => 'DESC']);
    $minCreated = $latestOrder ? $latestOrder->getOrderTime() : null;
    $minCreated = null;
    $offset = !is_null($this->currentOrder->getCurrentResultCount()) ? $this->currentOrder->getCurrentResultCount() : 0;
    $loadedResultCount = 0;
    $this->currentOrder
      ->setProcessingStartTime(time())
      ->setIsProcessing(true);
    $this->em->flush();
    do{
      $latestOrders = $etsy->getLatestOrders($minCreated, $offset);
      $offset += 50;
      $orders = $latestOrders->results;
      $loadedResultCount += count($orders);
      if(!$this->currentOrder->getTotalResultCount()){
        $this->currentOrder->setTotalResultCount($latestOrders->count);
      }

      foreach($orders as $order){
        if($receiptRepository->find($order->receipt_id)){
          continue;
        }
        $receipt = new Receipt();
        $receipt->setUser($user);
        $receipt->setId($order->receipt_id);
        $receipt->setOrderId($order->order_id);
        $receipt->setOrderTime($order->creation_tsz);
        $receipt->setBuyerEtsyUsername($order->Buyer->login_name);
        $receipt->setBuyerEmail($order->buyer_email);
        $receipt->setBuyerMessage($order->message_from_buyer);
        $name = explode(' ', $order->name);
        $receipt->setFirstName($name[0]);
        $receipt->setLastName(isset($name[1])? $name[1] : '');
        $receipt->setAddress1($order->first_line);
        $receipt->setAddress2($order->second_line);
        $receipt->setCity($order->city);
        $receipt->setState($order->state);
        $receipt->setZip($order->zip);
        $receipt->setCountry($order->Country->name);
        $receipt->setCountryCode($order->Country->iso_country_code);

        $this->em->persist($receipt);

        foreach($order->Transactions as $data){
          $transaction = new Transaction();
          $transaction->setId($data->transaction_id);
          $transaction->setItemName($data->title);
          $transaction->setItemQuantity($data->quantity);
          $variations = [];
          foreach($data->variations as $variation){
            $variations[] = $variation->formatted_value;
          } 
          $transaction->setItemVariations(implode(' ', $variations));
          if (preg_match('/(\d+) (\d+)$/', $data->description, $numbers)){
            $model_id = $numbers[1]; 
            $material_id = $numbers[2]; 
          }else{
            $model_id = null;
            $material_id = null;
          }
          $transaction->setModelId($model_id);
          $transaction->setMaterialId($material_id);
          $image = !is_null($data->MainImage) ? $data->MainImage : $data->Listing->Images[0];
          $transaction->setImageUrl($image->url_fullxfull);
          $transaction->setReceipt($receipt);

          $this->em->persist($transaction);
        }
      }
      $this->currentOrder
        ->setCurrentResultCount($loadedResultCount);
      $this->em->flush();
    }while(count($latestOrders->results) > 0);
  }
}