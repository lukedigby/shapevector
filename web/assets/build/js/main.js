var SHAPEVECTOR = {
	init: function(){
		this.bindEvents();
		this.orders.processing.check();
	},

	orders: {
		processing: {
			lastResultCount: 0,
			totalResultCount: 0,

			dom: {
				alertContainer: $('.alert-container')  
			},

			getAlert: function(){
				if(!this.dom.alert){
					this.dom.alert = this.dom.alertContainer.find('.alert-order-process');
				}
				return this.dom.alert;
			},

			check: function(){
				if(window.processing_orders){ this.checkServer(); }
			},

			checkServer: function(){
				if(this.dom.alertContainer.find('.alert-info').length == 0){
					$('<div/>')
					.append('<p>Initialising order fetching process...</p>')
					.append('<p class="results"></p>')
					.addClass('alert-order-process alert alert-info alert-dismissible')
					.appendTo(this.dom.alertContainer);
				}

				var parent = this;

				$.ajax('getProcessingOrderStatus',
					{
						data: {
							lastResultCount: this.lastResultCount
						}
					})
					.done(function(data){
						console.log(data);
						parent.showProcessingStatus(JSON.parse(data));
					});
			},

			showProcessingStatus: function(data)
			{
				$alert = this.getAlert();
				if(data.status == 'no processing orders'){
					$alert.prepend($('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'));
					if(this.lastResultCount < this.totalResultCount){
						$alert.append('<p>There was an error in loading the orders. Please try again later.</p>');
					}else{
						$alert.append('<p>Completed.</p>');
					}
					return;
				}
				if(data.processed_results != null){
					this.lastResultCount = data.processed_results;
					this.totalResultCount = data.total_results;
					var text = "Processed " + data.processed_results + "/" + data.total_results + 
						" results.";
					$alert.children('.results').text(text);
				}
				this.checkServer();
			}
		}
	},

	bindEvents: function(){
		$('.transaction-row .table').find('input, textarea').not('.tags input').change(function(){
			$form = $(this).parents('.transaction-row').find('.save-order-form');
			saveOrder($form);
		});

		$('.save-order-form').submit(function(e){
			e.preventDefault();
			saveOrder($(this));
		});

		$('.btn-upload-model input').change(function(){
			uploadModel(this);
		});

		$('.btn-make-order').click(function(){
			makeOrder(this);
		});

		$('.tags .label-add').click(function(){
			$(this).addClass('active');
			$(this).find('input').focus();
		});

		$('.tags .label-add i').click(function(e){
			e.stopPropagation();	
			var tagName = $(this).siblings('input').val();
			if(tagName !== ''){
				addTag($(this).parents('td'),tagName);
				$(this).siblings('input').val('');
				saveOrder($(this).parents('.transaction-row').find('.save-order-form'));
			}
			$(this).parents('.label-add').removeClass('active');
		});

		$('.tags').on('click', '.fa-remove', function(){
			$form = $(this).parents('.transaction-row').find('.save-order-form')
			$(this).parents('.label').remove();
			saveOrder($form);
		});

		$('.tags input').each(function(i, el) {
			var $el = $(el);
			$el.autocomplete({
				source: function(request, response) {
					$.ajax({
						dataType: "json",
						type: "GET",
						url: "/v2/getMatchingTags",
						data: {
							query: request.term
						},
						complete: function(data) {
							response(JSON.parse(data.responseText));
						}
					})
				},
				select: function(e, ui){
					addTag($el.parents('.tags td'), ui.item.label, ui.item.id);
					$el.siblings('input').val('');
					$el.parents('.label-add').removeClass('active');
					return false;
				}
			});
		});

		$('.get-csv').click(function(){
			s = location.search;
			$('<a>')
				.attr('download', true)
				.attr('href', 'http://www.shapevector.com/getCsv' + s)[0]
				.click();
		});

		$('#clear').click(function(){
			$(this).parents('form').find('input[type="text"], input[type="date"]').val('');
			return true;
		});

		//$('.btn-place-selected-orders').click(makeOrders);
		$('.btn-place-order').click(function(){
	    makeOrder(this);
	  });
	}
};

SHAPEVECTOR.init();

function addTag($list, tagName, id)
{
	if(id == undefined){
		id = null;
	}
	$list.find('.label-add').before('<span data-id="'+id+'" class="label label-primary">'+tagName+' <i class="fa fa-remove"></i></span> ');
	saveOrder($list.parents('.transaction-row').find('.save-order-form'));
}

function makeOrder(input){
	var $row = $(input).parents('.transaction-row');
	var $orderForm = $row.find('.save-order-form');

	var modelId = $row.find('[name="model_id"]').val();
	var materialId = $row.find('[name="material_id"]').val();

	var $table = $row.find('[id^=address]');

	var $shipDateField =  $row.find('input[name="target_shipping_date"]');
	var $shapewaysOrderNumber = $row.find('input[name="shapeways_order_number"]');

	var formData = new FormData();
	var name = $table.find('.name').text();
	if(name.indexOf(' ') != -1){
		var firstName = name.substr(0, name.indexOf(' '));
		var lastName = name.substr(name.indexOf(' ') + 1);
	}else{
		var firstName = name;
		var lastName = 'X';
	}

	formData.append('firstName', firstName);
	formData.append('lastName', lastName);
	formData.append('country', $table.find('.country').data('iso'));
	formData.append('state', $table.find('input[name="state"]').val());
	formData.append('city', $table.find('.city input').val());
	formData.append('address1', $table.find('.address1 input').val());
	formData.append('address2', $table.find('.address2 input').val());
	formData.append('zipCode', $table.find('.zip input').val());
	var item = {
		modelId: modelId,
		materialId: materialId,
		quantity: 1
	};
	var items = [item];
	formData.append('items', JSON.stringify(items));
	showLoading('Sending to Shapeways...');
	$.ajax({
		type: 'POST',
		url: '/makeOrder',
		data: formData,
		success: function(data){
			response = JSON.parse(data);
			console.log(response);

			if(response.result == "failure") {
				hideLoading();
				alert(response.reason);
			} else {
				$shipDateField.val(response.targetShipDate);
				$shipDateField.before(response.formattedDate);

				var number = response.orderId;
				$shapewaysOrderNumber.val(number);
				$anchor = $shapewaysOrderNumber.prev('a');
				$anchor.text(number).attr('href', $anchor.attr('href')+number);
				hideLoading();
				saveOrder($orderForm);
			}
		},
		contentType: false,
		processData: false
	});
}

function makeOrders()
{
	$('.selected-order:checked').each(function(){
		makeOrder(this);
	})
}

function uploadModel(input)
{
	var $form = $(input).parents('form');
	var $orderForm = $form.siblings('.save-order-form');
	var formData = new FormData($form[0]);
	var $modelField =  $(input).parents('.transaction-row').find('input[name="model_id"]');
	showLoading('Uploading Model...');
	$.ajax({
		type: 'POST',
		url: $form.attr('action'),
		data: formData,
		success: function(data){
			$modelField.val(data.modelId);
			hideLoading();
			saveOrder($orderForm);
		},
		contentType: false,
		processData: false
	});
}

function saveOrder($form)
{
	var $inputs = $form.parents('.transaction-row').find('.table').find('input, textarea').clone().css({'position': 'absolute', 'opacity': 0});
	$inputs.filter(':disabled').prop('disabled', false);
	$inputs.appendTo($form);
	var formData = new FormData($form[0]);
	var tags = [];
	$form.parents('.transaction-row').find('.tags span:not(.label-add)').each(function(){
		tags.push({
			id: $(this).data('id'),
		 	name: $(this).text()
		 });
	});
	if(tags.length > 0) {
		formData.append('tags', JSON.stringify(tags));
	}

	showLoading('Saving Order...');
	$.ajax({
		type: 'POST',
		url: $form.attr('action'),
		data: formData,
		complete: function(data){
			console.log(data);
			$form.find('[name="id"]').val(JSON.parse(data.responseText).id);
			hideLoading();
		},
		contentType: false,
		processData: false
	});
}

function showLoading(msg)
{
	if(msg){
		$('#loading .message').text(msg);
	}else{
		$('#loading .message').text('');
	}
	$('#loading').show();
}

function hideLoading()
{
	$('#loading').hide();
}