import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, Link, browserHistory, IndexRoute} from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Layout from './Layout';
import Orders from './Pages/Orders';
import Config from './Pages/Config';
import Settings from './Pages/Settings';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
import Perf from 'react-addons-perf';

if (process.env.NODE_ENV !== 'production') {
  window.ReactPerf = Perf;
}

ReactDOM.render(
	<MuiThemeProvider>
		<Router history={browserHistory}>
			<Route path="/(app_dev.php)" component={Layout}>
				<IndexRoute component={Orders} />
				<Route path="config" component={Config} />
				<Route path="settings" component={Settings} />
			</Route>
		</Router>
	</MuiThemeProvider>,
	document.getElementById('root')
);