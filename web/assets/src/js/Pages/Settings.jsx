import RaisedButton from 'material-ui/RaisedButton';
import TagList from '../Components/TagList';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import FontIcon from 'material-ui/FontIcon';
import update from 'react-addons-update';

export default React.createClass({
	getInitialState: function() {
		this.paths = {
			get_settings: this.context.Routing.generate('get_settings', {}, true),
			post_tag_rule: this.context.Routing.generate('post_tag_rule', {}, true),
			put_tag_rule: this.context.Routing.generate('put_tag_rule', {}, true),
			delete_tag_rule: this.context.Routing.generate('delete_tag_rule', {}, true),
			get_tags: this.context.Routing.generate('get_tags', {}, true),
			post_tag: this.context.Routing.generate('post_tag', {}, true),
		};
		return {
			tagRules: [],
			globalRule: null,
			tagSourceData: [],
			loadedTags: false,
			focusedInputValue: ''
		};
	},

	contextTypes: {
		Routing: 	React.PropTypes.object.isRequired,
		showFlash: React.PropTypes.func.isRequired
	},

	componentWillMount: function() {
		$.ajax(this.paths.get_settings, {
			method: 'GET',
			success: function (settings) {
				console.log(settings);
				this.setState({	
					tagRules: settings.tag_rules,
					globalRule: settings.global_rule
				});
			}.bind(this),
			xhrFields: {
				withCredentials: true
			}
		});

		$.ajax(this.paths.get_tags, {
			method: 'GET',
			success: function (tags) {
				console.log(tags);
				this.setState({	
					tagSourceData: tags,
					loadedTags: true
				});
			}.bind(this),
			xhrFields: {
				withCredentials: true
			}
		});
	},


	_onBlur: function(index){
		return function(e) {
			if(e.target.value != this.state.focusedInputValue){
				this._checkForUpdate(index);
			}
		}.bind(this)
	},
	_onFocus(e) {
		this.setState({
			focusedInputValue: e.target.value
		});
	},

	_getRule(index){
		return index == 'global' ? this.state.globalRule : this.state.tagRules[index];
	},

	_clearGlobalRule(){
		if(!this.state.globalRule.id){
			this.context.showFlash('Tag deleted Sucessfully.');
		}else{
			this.context.showFlash('Deleting Tag Rule...');
			$.ajax({
				url: this.paths.delete_tag_rule,
				data: this.state.globalRule,
				method: 'DELETE',
				xhrFields: {
					withCredentials: true
				},
				complete: function(xhr){
					if(xhr.responseJSON.status == 'success'){
						this.context.showFlash('Tag deleted Sucessfully.');
						this.setState({
							tagRules: update(this.state.tagRules, {id:{ $set: null}})
						});
					}else{
						this.context.showFlash('Tag deletion Failed.');
					}
				}.bind(this)
			});
		}
	},

	_checkForUpdate(index){
		var rule = this._getRule(index);
		if(index == 'global'){
			this._postTagRule(index);
		}else if((rule.field && rule.value && rule.tag && rule.tag.id)){
			if(rule.id){
				this._putTagRule(index);
			}else{
				this._postTagRule(index);
			}
		}
	},

	_postTagRule: function(index) {
		var rule = this._getRule(index);
		this.context.showFlash('Adding Tag Rule...');
		$.ajax({
			url: this.paths.post_tag_rule,
			data: rule,
			method: 'POST',
			xhrFields: {
				withCredentials: true
			},
			complete: function(xhr){
				var id = xhr.responseJSON.id;
				if(index =='global'){
					var newState = 	{globalRule: update(this.state.globalRule, { id:{ $set: id }})}
				}else{
					var newState = 	{tagRules: update(this.state.tagRules, {[index]:{ id:{ $set: id }}})}
				}
				this.setState(newState)	;
				if(xhr.responseJSON.status == 'success'){
					this.context.showFlash('Tag Rule Added Successfully.');
				}else{
					this.context.showFlash('Tag Rule Addition Failed.');
				}
			}.bind(this)
		});
	},

	_putTagRule: function(index) {
		var rule = this._getRule(index);
		this.context.showFlash('Updating Tag Rule...');
		$.ajax({
			url: this.paths.put_tag_rule,
			data: rule,
			method: 'PUT',
			xhrFields: {
				withCredentials: true
			},
			complete: function(xhr){
				if(xhr.responseJSON.status == 'success'){
					this.context.showFlash('Tag Rule updated Sucessfully.');
				}else{
					this.context.showFlash('Tag Rule update Failed.');
				}
			}.bind(this)
		});
	},

	_addTagRule: function() {
		this.setState({
			tagRules: update(this.state.tagRules, {$push: [{
				field: '',
				value: '',
				tag: null
			}]})
		});
	},

	_removeTagRule: function(index) {
		return function(){
			this.context.showFlash('Deleting Tag Rule...');
			var rule = this.state.tagRules[index];
			this.setState({
				tagRules: update(this.state.tagRules, {[index]:{ removing:{ $set: true}}})
			});
			if(!rule.id){
				this.context.showFlash('Tag Rule deleted Sucessfully.');
				this.setState({
					tagRules: update(this.state.tagRules, {$splice: [[[index], 1]]})
				});
			}else{
				$.ajax({
					url: this.paths.delete_tag_rule,
					data: rule,
					method: 'DELETE',
					xhrFields: {
						withCredentials: true
					},
					complete: function(xhr){
						if(xhr.responseJSON.status == 'success'){
							this.context.showFlash('Tag Rule deleted Sucessfully.');
							this.setState({
								tagRules: update(this.state.tagRules, {$splice: [[[index], 1]]})
							});
						}else{
							this.context.showFlash('Tag Rule deletion Failed.');
							this.setState({
								tagRules: update(this.state.tagRules, {[index]:{ removing:{ $set: false}}})
							});
						}
					}.bind(this)
				});
			}
		}.bind(this)
	},

	_updateField(index){
		return function(e, key, payload){
			var value = payload;
			this.setState({
				tagRules: update(this.state.tagRules, {[index]: {field: {$set: value}}})
			});
		}.bind(this)
	},
	_updateValue(index){
		return function(e){
			var value = e.target.value;
			this.setState({
				tagRules: update(this.state.tagRules, {[index]: {value: {$set: value}}})
			});
		}.bind(this)
	},
	_addTag(tag, index){
		if(index =='global'){
			this.setState({
				globalRule: update(this.state.globalRule, {tag:{ $set: tag}})
			}, () => this._checkForUpdate(index));
		}else{
			this.setState({
				tagRules: update(this.state.tagRules, {[index]:{ tag:{ $set: tag }}})
			}, () => this._checkForUpdate(index));
		}
	},

	_onRequestAddTag(index) {
		return function(name){
			var tag = this.state.tagSourceData.filter(tag => tag.name === name)[0];
			if(tag){ // This is an existing tag
				// Update this component with the new tag
				this._addTag(tag, index);
			}else{ // Send a request to the server to create the tag for us
				$.ajax({
					url: this.paths.post_tag,
					method: 'POST',
					data: {name: name},
					xhrFields: {
						withCredentials: true
					},
					success: function(data){
						// Call the parent handler to add the new tag globally
						this._onCreateNewTag(data);
						// Update this component with the new tag
						this._addTag(data, index);
						console.log(data);
					}.bind(this)
				});
			}
		}.bind(this)
	},

	_onCreateNewTag: function(data) {
		this.setState({
			tagSourceData: update(this.state.tagSourceData, {$push: [data]})
		});
	},

  _onDeleteTag: function(index) {
  	return function(id){
			this.setState({
				tagRules: update(this.state.tagRules, {[index]: {tag: {$set: null}}})
			});
  	}.bind(this)
  },

  _onDeleteGlobalTag: function() {
  	this.setState({
			globalRule: update(this.state.globalRule, {tag: {$set: null}})
		}, this._clearGlobalRule);
  },

	_getTags(rule) {
		return this.state.tagSourceData.filter(tag => rule.tags.indexOf(tag.id) != -1);
	},

	render: function() {
		return (
			<div>
				<h2>Tagging Rules</h2>
				<div className="form-group form-inline tag-rule global">
					Tag all new orders with: 
					<TagList
						style={{display: 'inline-block'}}
						tags={this.state.globalRule && this.state.globalRule.tag ? [this.state.globalRule.tag] : []}
						sourceData={this.state.tagSourceData}
						loadedTags={this.state.loadedTags}
						maxTags={1}
						onRequestAddTag={this._onRequestAddTag('global')}
						onDeleteTag={this._onDeleteGlobalTag}
					/>
				</div>
				{this.state.tagRules.map(function(rule, index){
					return (
						<div 
							key={index} 
							className="form-group form-inline tag-rule"
							onBlur={this._onBlur(index)}
							onFocus={this._onFocus}
						>
							<SelectField 
								floatingLabelText="Field type"
								value={rule.field} 
								onChange={this._updateField(index)}
							>
					      <MenuItem value="name" primaryText="Item Name" />
					      <MenuItem value="variations" primaryText="Variations" />
					    </SelectField>
							<TextField 
								floatingLabelText="Contains"
								value={rule.value}
								onChange={this._updateValue(index)}
							/>
							<TagList
								style={{display: 'inline-block'}}
								tags={rule.tag ? [rule.tag] : []}
								sourceData={this.state.tagSourceData}
								loadedTags={this.state.loadedTags}
								maxTags={1}
								onRequestAddTag={this._onRequestAddTag(index)}
								onDeleteTag={this._onDeleteTag(index)}
							/>
							<RaisedButton
      					icon={rule.removing ? <FontIcon className="fa fa-spin fa-refresh" /> : ''}			
								label="Remove Rule"
								onTouchTap={this._removeTagRule(index)}
							/>
						</div>
					);
				}, this)}
				<RaisedButton 
					label="Add Rule" 
					onTouchTap={this._addTagRule}
					style={{whiteSpace: 'nowrap'}}
				/>
			</div>		
		);
	}
})