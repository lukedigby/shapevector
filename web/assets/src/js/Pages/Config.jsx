import RaisedButton from 'material-ui/RaisedButton';
export default React.createClass({
	getInitialState: function() {
		this.paths = {
			shopify_connect: this.context.Routing.generate('shopify_connect', {}, true),
			etsy_connect: this.context.Routing.generate('etsy_connect', {}, true), 
			shapeways_connect: this.context.Routing.generate('shapeways_connect', {}, true),
			update_config: this.context.Routing.generate('update_config', {}, true)
		};
		return {
			shopifyShopName: app.user.shopifyShopName
		};
	},

	contextTypes: {
		Routing: 	React.PropTypes.object.isRequired,
		showFlash: React.PropTypes.func.isRequired
	},

	_saveConfig: function() {
		this.context.showFlash('Saving...')
		$.ajax({
			url: this.paths.update_config,
			data: this.state,
			method: 'PUT',
			xhrFields: {
				withCredentials: true
			},
			complete: function(xhr){
				if(xhr.responseJSON.status == 'success'){
					this.context.showFlash('Configuration Saved Sucessfully.');
				}else{
					this.context.showFlash('Save Failed.');
				}
			}.bind(this)
		})
	},

	_linkState: function(key) {
		return e => {
			const value = e.target.value;
			this.setState({
				[key]: value
			});
		}
	},

	_onBlur: function(e) {
		if(e.target.value != this.state.focusedInputValue){
			this._saveConfig();
		}
	},
	_onFocus(e) {
		this.setState({
			focusedInputValue: e.target.value
		});
	},

	render: function() {
		var buttonStyle = {marginLeft: '20px'};
		return (
			<div>
				<div>
						<span>Shopify connection status: {app.user.shopifyToken ? 'Connected' : 'Not Connected'}.</span>
						<RaisedButton style={buttonStyle} href={this.paths.shopify_connect} label={app.user.shopifyToken ? 'Reconnect' : 'Connect'} />
				</div>
				<br/>
				<div>
						<span>Etsy connection status: {app.user.etsyTokenSecret ? 'Connected' : 'Not Connected'}.</span>
						<RaisedButton style={buttonStyle} href={this.paths.etsy_connect} label={app.user.etsyTokenSecret ? 'Reconnect' : 'Connect'} />
				</div>
				<br/>
				<div>
						<span>Shapeways connection status: {app.user.shapewaysTokenSecret ? 'Connected' : 'Not Connected'}.</span>
						<RaisedButton style={buttonStyle} href={this.paths.shapeways_connect} label={app.user.shapewaysTokenSecret ? 'Reconnect' : 'Connect'} />
				</div>
				<br/>
				<div 
					className="form-group"
					onBlur={this._onBlur} 
					onFocus={this._onFocus} 
				>
					<label htmlFor="shopify_shop_name">Shopify shop name</label>
					<input 
						name="shopify_shop_name" 
						className="form-control" 
						value={ this.state.shopifyShopName } 
						onChange={ this._linkState('shopifyShopName') }
					/>
				</div>
			</div>		
		);
	}
})