import OrderList from '../Components/OrderList';
import Filters from '../Components/Filters';
import Pagination from '../Components/Pagination';
import Loader from '../Components/Loader';
import update from 'react-addons-update';
import moment from 'moment';

export default React.createClass({
	getInitialState: function() {
		this.paths = {
			getReceipts: this.context.Routing.generate('get_receipts', {}, true),
			getTags: this.context.Routing.generate('get_tags', {}, true),
			postTag: this.context.Routing.generate('post_tag', {}, true),
			getMaterials: this.context.Routing.generate('get_shapewaysmaterials', {}, true),
		};
		this.receipts = [];
		this.reversedReceipts = [];
		return {
			loading: true,
			pageNumber: 1,
			filters: {
				minDate: null,
				maxDate: null,
				search: null,
				pageSize: 5,
				tag: null,
				orderDir: 'desc'
			},
			pagedReceipts: [],
			visibleReceipts: [],
			visibleReceiptCount: 0
		}
	},

	contextTypes: {
		Routing: 	React.PropTypes.object.isRequired,
		showFlash: React.PropTypes.func.isRequired
	},

	componentWillMount: function() {
		$.ajax(this.paths.getReceipts, {
			success: function (receipts) {
				this.receipts = receipts;
				this.reversedReceipts = Array.prototype.slice.call(receipts);
				this.reversedReceipts.reverse();
				this.setState({	
					loading: false
				});
				this._getPagedReceipts();
			}.bind(this),
			xhrFields: {
				withCredentials: true
			}
		});
	},

	_updateFilterState: function(filterName, val) {
		var newState = update(this.state, {
			filters:{
				[filterName]: {$set: val}
			},
			pageNumber: {$set: 1}
		});
		this.setState(newState, this._getPagedReceipts);
	},
	_onMinDateChange: function(empty, date) {
		this._updateFilterState('minDate', date);
	},
	_onMaxDateChange: function(empty, date) {
		this._updateFilterState('maxDate', date);
	},
	_onPageSizeChange: function(e) {
		this._updateFilterState('pageSize', e.target.value);
	},
	_onSearchChange: function(value) {
		this._updateFilterState('search', value);
	},
	_onOrderDirChange: function(value) {
		this._updateFilterState('orderDir', value);
	},
	_onTagFilterChange: function(id) {
		if(this.state.filters.tag == id){
			id = null;
		}
		this._updateFilterState('tag', id);
	},
	_paginate: function(pageNumber) {
		this.setState({
			pageNumber: pageNumber
		},
		this._getPagedReceipts);
	},
	_getFilters: function() {
		const {filters} = this.state;
		var minDate = filters.minDate ? moment(filters.minDate).set({'second': 0, 'minute': 0, 'hour': 0}).unix() : null;
		var maxDate = filters.maxDate ? moment(filters.maxDate).set({'second': 0, 'minute': 0, 'hour': 24}).unix() : null;
		var filterFunctions = [];
		if(minDate){ filterFunctions.push(receipt => receipt.orderTime >= minDate) }
		if(maxDate){ filterFunctions.push(receipt => receipt.orderTime <= maxDate) }
		if(filters.search){ filterFunctions.push(receipt => receipt.orderId ? receipt.orderId.toString().substring(0, filters.search.length) == filters.search : false) }
		if(filters.tag){ 
			filterFunctions.push(receipt => {
				return receipt.transactions.some(transaction => {
					return transaction.tags.some(tag => {
						return tag == filters.tag;
					});
				});
			}); 
		}
		return filterFunctions;
	},
	_getOrderedReceipts: function() {
		return this.state.filters.orderDir == 'asc' ?
		this.reversedReceipts :
		this.receipts;
	},
	_getPagedReceipts: function() {
		const {filters,pageNumber} = this.state;

		let visibleReceipts = this._getOrderedReceipts()
			.filter(receipt =>{
				return this._getFilters().every(filter => {
					return filter(receipt);
				})
			});

		var pagedReceipts = visibleReceipts
			.slice((pageNumber - 1) * filters.pageSize, pageNumber * filters.pageSize);
		this.setState({
			visibleReceipts: visibleReceipts,
			visibleReceiptCount: visibleReceipts.length,
			pagedReceipts: pagedReceipts
		});
	},

	render: function() { 
		const loader = this.state.loading ? 
	    <Loader /> : 
			'';
		return (
			<div>
				{loader}
				<h1>Latest Orders</h1>
				<Filters 
					{...this.state.filters} 
					onMinDateChange={this._onMinDateChange} 
					onMaxDateChange={this._onMaxDateChange} 
					onPageSizeChange={this._onPageSizeChange} 
					onSearchChange={this._onSearchChange} 
					onOrderDirChange={this._onOrderDirChange}
				/>
				<Pagination 
					pageNumber={this.state.pageNumber} 
					pageSize={this.state.filters.pageSize} 
					totalResults={this.state.visibleReceiptCount} 
					onPageChange={this._paginate}
				/>
				<OrderList 
					{...this.state} 
					visibleReceipts={this.state.visibleReceipts}
					pagedReceipts={this.state.pagedReceipts} 
					paths={this.paths} 
					showFlash={this.context.showFlash}
					onTagFilterChange={this._onTagFilterChange}
				/>
				<Pagination 
					pageNumber={this.state.pageNumber} 
					pageSize={this.state.filters.pageSize} 
					totalResults={this.state.visibleReceiptCount} 
					onPageChange={this._paginate}
				/>
			</div>
		);
	}
});