import MainNav from '../Components/MainNav';	
import FlashContainer from '../Components/FlashContainer';

// Make Routing variable available
var Routing = require('routing');

export default React.createClass({
	getInitialState: function() {
		return {
			flash: '',
			persistFlash: false,
			flashTimeout: null
		}
	},

	_showFlash: function(message, persist = false) {
		var component = this;
		this.setState(
			{
				flash: message,
				persistFlash: persist
			}, 
			function() {
				if(!persist){
					clearTimeout(this.state.flashTimeout);
					var timeoutHandle = setTimeout(function(){
						component.setState({
							flash: ''
						});	
					}, 2000);
					component.setState({
						flashTimeout: timeoutHandle
					});
				}
			}
		);
	},

	_onFlashClose: function() {
		this.setState({
			flash: '',
			persistFlash: false
		});
	},

	getChildContext: function() {
		return {
			Routing: Routing,
			showFlash: this._showFlash,
		}
	},

	childContextTypes: {
		Routing: React.PropTypes.object.isRequired,
		showFlash: React.PropTypes.func.isRequired
	},

	render: function(){
		return (
			<div>
				<MainNav />
			  <div className="container" style={{paddingTop: '20px', paddingBottom: '20px' }}>
			    <div className="row">
			      <div className="col-md-12">	
							<FlashContainer 
								message={this.state.flash} 
								persist={this.state.persistFlash} 
								onClose={this._onFlashClose}
							/>
							{this.props.children}
						</div>
					</div>
				</div>
			</div>
		);
	} 
})

