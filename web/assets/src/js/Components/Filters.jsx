import moment from 'moment';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

export default React.createClass({
	getInitialState: function(){
		this.searchTimeout = null;
		return {
			search: '',
			orderDir: 'desc'
		};
	},
	onSearchChange: function(e){
		this.setState({search: e.target.value.toString() });
	},
	onSearchKeyUp: function(e){
		clearTimeout(this.searchTimeout);
		if(e.keyCode == 13){
			this.updateSearch();
		}else{
			this.searchTimeout = setTimeout(this.updateSearch, 200);
		}
	},
	updateSearch: function(){
		this.props.onSearchChange(this.refs.searchInput.getValue());
	},
	_setOrderDir: function(dir){
		return function(){
			this.setState({
				orderDir: dir
			});
			this.props.onOrderDirChange(dir);
		}.bind(this)
	},
	_activeStyle: {
		backgroundColor: 'rgba(153, 153, 153, 0.3)'
	},

	render: function() {
		return (
			<div className="row">
				<div className="col-xs-12 form-inline">
					{/*<div className="form-group">
			    	<DatePicker
			    		autoOk={true}
			    		hintText="Date From" 
			    		container="inline"
			    		name="min_date" 
			    		format="yyy-mm-dd" 
			    		onChange={this.props.onMinDateChange}
			    		value={this.props.minDate || {}}
			    	/>
			  	</div>
					<div className="form-group">
						<DatePicker
			    		autoOk={true}
							hintText="Date To"
			    		container="inline"
							name="max_date" 
							format="yyy-mm-dd" 
							onChange={this.props.onMaxDateChange}
							value={this.props.maxDate || {}}
						/>
		  		</div>*/}
					<div className="form-group">
						<TextField 
							name="search" 
							hintText="Order ID"
							ref="searchInput"
							onChange={this.onSearchChange} 
							onKeyUp={this.onSearchKeyUp}
							onBlur={this.updateSearch}
							value={this.state.search} 
						/>
					</div>
					<div className="form-group">
						<FlatButton 
							style={this.state.orderDir == 'asc' ? this._activeStyle : {}}
							label="Asc" 
							onClick={this._setOrderDir('asc')} 
						/>
						<FlatButton 
							style={this.state.orderDir == 'desc' ? this._activeStyle : {}}
							label="Desc"
							onClick={this._setOrderDir('desc')}
						/>
					</div>
				</div>
			</div>
		);
	}
});