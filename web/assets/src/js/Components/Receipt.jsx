import Transaction from './Transaction';
import Panels from './TransactionPanels';
import UploadModelForm from './UploadModelForm';
import PlaceShapewaysOrderButton from './PlaceShapewaysOrderButton';
import CancelShapewaysOrderButton from './CancelShapewaysOrderButton';
import Divider from 'material-ui/Divider';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from './TransactionDataTableTextField';
import TagList from './TagList';
import Table from './TransactionDataTable';
import Row from './TransactionDataTableRow';
import moment from 'moment';
import shallowCompare from 'react-addons-shallow-compare';
import update from 'react-addons-update';

export default React.createClass({
	getInitialState: function(){
		var targetShippingDate = this.props.receipt.targetShippingDate ? this.props.receipt.targetShippingDate.timestamp : null;
		return {
			firstName: this.props.receipt.firstName || '',
			lastName: this.props.receipt.lastName || '',
			address1: this.props.receipt.address1 || '',
			address2: this.props.receipt.address2 || '',
			city: this.props.receipt.city || '',
			state: this.props.receipt.state || '',
			zip: this.props.receipt.zip || '',
			country: this.props.receipt.country || '',
			countryCode: this.props.receipt.countryCode || '',
			transactions: this.props.receipt.transactions.map(function(transaction){
				['modelId', 'modelName', 'materialId', 'orderNotes', 'tags'].map(function(field){
					transaction[field] = transaction[field] || '';
				});	
				transaction['extraDetails'] = transaction['extraDetails'] || [];
				return transaction;
			}),
			shapewaysOrderNumber: this.props.receipt.shapewaysOrderNumber || '',
			targetShippingDate: targetShippingDate,
			placingOrder: false,
			cancellingOrder: false,
		};
	},

	contextTypes: {
		Routing: 	React.PropTypes.object.isRequired,
		showFlash: React.PropTypes.func.isRequired,
	},

	_onBlur: function(index){
		return function(e) {
			if(e.target.value != this.state.focusedInputValue){
				this._saveOrder(index);
			}
		}.bind(this)
	},
	_onFocus(e) {
		this.setState({
			focusedInputValue: e.target.value
		});
	},

	_updateTransactionState: function(field, index, save){
		return (e, key, payload) => {
			var value = payload ? payload : e.target.value;
			this.setState({
				transactions: update(this.state.transactions, {[index]: { [field]: { $set: value}}})
			}, ()=>{
				if(save){
					this._saveOrder(index);
				}
			});
		}
	},
	_updateReceiptState: function(field){
		return e => {
			var value = e.target.value;
			this.setState(
				update(this.state, {[field]: { $set: value}})
			);
		}
	},

	_onRequestAddTag(index) {
		return function(name){
			var tag = this.props.tagSourceData.filter(tag => tag.name === name)[0];
			if(tag){ // This is an existing tag
				// Update this component with the new tag id
				this._addTag(tag.id, index);
			}else{ // Send a request to the server to create the tag for us
				$.ajax({
					url: this.props.paths.postTag,
					method: 'POST',
					data: {name: name},
					xhrFields: {
						withCredentials: true
					},
					success: function(data){
						// Call the parent handler to add the new tag globally
						this.props.onCreateNewTag(data);
						// Update this component with the new tag id
						this._addTag(data.id, index);
						console.log(data);
					}.bind(this)
				});
			}
		}.bind(this)
	},
	_addTag: function(id, index) {
		// Add the tag to the state for this transaction
		if(this.state.transactions[index].tags.indexOf(id) != -1){ return; }
		this.setState({
			transactions: update(this.state.transactions,{[index]: {tags: {$push: [id] }}})
		});
		this._saveOrder(index);
	},
  _onDeleteTag: function(index) {
  	return function(id){
	  	var tagIndex = this.state.transactions[index].tags.indexOf(id);
			this.setState({
				transactions: update(this.state.transactions, {[index]: {tags: {$splice: [[tagIndex, 1]]}}})
			}, function(){
				this._saveOrder(index);
			});
  	}.bind(this)
  },
	_getActiveTags: function(index){
		return this.props.tagSourceData.filter(t => this.state.transactions[index].tags.indexOf(t.id) != -1);
	},

 	_getSaveOrderUrl: function(index) {
		return this.context.Routing.generate(
			'put_transaction',
			{
				transaction_id: this.state.transactions[index].id
			},
			true);
	},
	_prepareSaveData(index){
		return Object.assign({}, this.state, this.state.transactions[index]);
	},
	_saveOrder: function(index) {
		this.context.showFlash('Saving...');
		$.ajax({
			url: this._getSaveOrderUrl(index),
			method: 'PUT',
			data: this._prepareSaveData(index),
			xhrFields: {
				withCredentials: true
			},
			complete: function(xhr, status){
				this.context.showFlash('Saved.');
			}.bind(this)
		});
	},
	_onUploadModel: function(index){
		return function(data) {
			this.setState({
				transactions: update(this.state.transactions, {[index]: {
					modelId: {$set: data.modelId},
					modelName: {$set: data.modelName}
				}})
			}, function(){this._saveOrder(index)});
		}.bind(this)
	},

  _placeShapewaysOrder: function() {
		this.setState({
			placingOrder: true
		});
		var data = {
			firstName: this.props.receipt.firstName,
			lastName: this.props.receipt.lastName,
			state: this.state.state,
			city: this.state.city,
			country: this.state.countryCode,
			address1: this.state.address1,
			address2: this.state.address2,
			zipCode: this.state.zip,
			items: []
		};
		this.state.transactions.map(function(transaction){
			if(transaction.modelId && transaction.materialId){
				data.items.push({
					modelId: transaction.modelId,
					materialId: transaction.materialId,
					quantity: transaction.itemQuantity
				});
			}
		});

		$.ajax({
			url: this.context.Routing.generate('post_shapewaysorder', {}, true),
			method: 'POST',
			data: data,
			xhrFields: {
				withCredentials: true
			},
			success: function(data){
				console.log(data);
				if(data.response){
					var newState = {
	  				targetShippingDate: moment(data.response.targetShippingDate).unix(),
	  				shapewaysOrderNumber: data.response.orderId,
					};  	
				}else{
					this.context.showFlash('Order failed. Please try again.');
				}	
				newState.placingOrder = false;
				this.setState(newState, () => this._saveOrder(0));
			}.bind(this) 
		})
	},
	_cancelShapewaysOrder: function() {
		this.setState({
			cancellingOrder: true
		});
		$.ajax({
			url: this.context.Routing.generate('put_shapewaysorder', {id: this.state.shapewaysOrderNumber}, true),
			method: 'PUT',
			data: {
				status: 'cancelled'
			},
			xhrFields: {
				withCredentials: true
			},
			success: function(response){
				console.log(response);
				if(response.result == "failed"){
					this.context.showFlash('Cancellation Unsuccesful - ' + response.reason, true);
				}else if(response.result == "success"){
					this.context.showFlash('Order cancelled successfully');
					this.setState({
						shapewaysOrderNumber: null,
						targetShippingDate: null,
						cancellingOrder: false
					}, this._saveOrder(0));
				}
			}.bind(this)
		});
	},

	shouldComponentUpdate: function(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	},

	render: function(){
		return (
			<div className="receipt">
			{this.state.transactions.map((transaction, index) =>
				<Transaction key={transaction.id} >
					<div className="col-xs-6 col-xs-offset-3 col-sm-offset-0 col-sm-3">
						<img className="img-responsive img-border" src={transaction.imageUrl} />
						<UploadModelForm onUploadModel={this._onUploadModel(index)} />
						<Divider style={{marginBottom: '10px'}} />
						{(transaction.modelId && transaction.materialId) ? 
							(this.state.shapewaysOrderNumber ? 
								<CancelShapewaysOrderButton cancelling={this.state.cancellingOrder} cancelShapewaysOrder={this._cancelShapewaysOrder} /> :
								<PlaceShapewaysOrderButton ordering={this.state.placingOrder} placeShapewaysOrder={this._placeShapewaysOrder} />
							) :	''}
					</div>
					<div className="col-xs-12 col-sm-9">
						<Tabs onBlur={this._onBlur(index)} onFocus={this._onFocus}>
							<Tab label="Key Details" value={"key" + transaction.id}>
								<Table>
									{/*<Row label="Etsy Username">
										{this.props.receipt.buyerEtsyUsername || ''}
										<a target="_blank" href={"http://www.etsy.com/conversations/new?with_id=" + this.props.receipt.buyerEtsyUsername || ''}>
											Send Etsy Message
										</a>
									</Row>*/}
									<Row label="Order ID">
										{this.props.receipt.orderId}&nbsp;
										<a target="_blank" href={"http://myshopify.com/admin/orders/" + this.props.receipt.id} className>
											View Order <i className="fa fa-external-link"></i>	
										</a>
									</Row>
									<Row label="Model" className="no-pad">
										<TextField 
											name="model_id" 
											value={transaction.modelId} 
											onChange={this._updateTransactionState('modelId', index)} 
											style={{width: 128}} 
										/>&nbsp;
										{(() => {
											if(transaction.modelId && transaction.modelId != "0"){
												return (
													<a target="_blank" href={"http://www.shapeways.com/model/upload-and-buy/" + transaction.modelId} className>
														View Model "{transaction.modelName}" <i className="fa fa-external-link"></i>	
													</a>
												);
											} 
										})()}
									</Row>
									<Row label="Material" className="no-pad">
											<SelectField 
												name="material_id" 
												value={transaction.materialId} 
												onChange={this._updateTransactionState('materialId', index, true)}
											>
										{Object.keys(this.props.materials).map(key => {
											return <MenuItem key={key} value={parseInt(key)} primaryText={this.props.materials[key].title} />
										})}
		    							</SelectField>
									</Row>
									<Row label="Shapeways order number">
										<TextField 
											name="shapeways_order_number" 
											value={this.state.shapewaysOrderNumber} 
											onChange={this._updateReceiptState('shapewaysOrderNumber', index)} 
											style={{width: 128}} 
										/>&nbsp;
											{(() => {
												if(this.state.shapewaysOrderNumber && this.state.shapewaysOrderNumber != 0){
													return (
														<span>
															<a target="_blank" href={"https://www.shapeways.com/checkout/receipt?orderId=" + this.state.shapewaysOrderNumber}>
																View order <i className="fa fa-external-link"></i>
															</a>
														</span>
													);
												}
											})()} 
									</Row>
									<Row label="Target Ship Date">
										{(()=>{
											if (this.state.targetShippingDate){
												let date = moment.unix(this.state.targetShippingDate).format('ddd, MMM D Y');
												return (
													<span>
														{date}
													</span>
												);
											}
										})()}
									</Row>
									<Row label="Message from buyer">
										{this.props.receipt.buyerMessage}
									</Row>
									<Row label="Item quantity">
										{transaction.itemQuantity}
									</Row>
									{transaction.extraDetails.map((item) => (
										<Row key={item.name+'-'+item.value} label={item.name}>
											{item.value}
										</Row>
									))}
									{/*<Row label="Variations">
										{this.props.itemVariations}
									</Row>*/}
									<Row label="Order Notes" className="no-pad">
										<TextField 
											name="notes" 
											multiLine={true}
											rows={1}
											rowsMax={2} 
											hintText="Write order notes here"
											value={transaction.orderNotes}
											onChange={this._updateTransactionState('orderNotes', index)}
										/>
									</Row>
									<Row label="Tags" className="no-pad">
										<TagList 
											tags={this._getActiveTags(index)} 
											onRequestAddTag={this._onRequestAddTag(index)} 
											onDeleteTag={this._onDeleteTag(index)} 
											sourceData={this.props.tagSourceData} 
											loadedTags={this.props.loadedTags} 
										/>
									</Row>
								</Table>
							</Tab>
							<Tab label="Order Details" value={"order" + transaction.id}>
								<Table>
									<Row label="Receipt ID">
										{this.props.receipt.id}
									</Row>
									<Row label="Transaction ID">
										{transaction.id}
									</Row>
									<Row label="Order time">
										{moment.unix(this.props.receipt.orderTime).format('ddd, MMM D Y h:mma')}
									</Row>
									<Row label="Item name">
										{transaction.itemName}
									</Row>
								</Table>
							</Tab>
							<Tab label="Address Details" value={"address" + transaction.id}>
								<Table>
									<Row label="First Name">
										<TextField 
											name="firstName" 
											value={this.state.firstName}
											onChange={this._updateReceiptState('firstName')}
										/>
									</Row>
									<Row label="Last Name">
										<TextField 
											name="lastName" 
											value={this.state.lastName}
											onChange={this._updateReceiptState('lastName')}
										/>
									</Row>
									<Row label="Address 1" className="no-pad">
										<TextField 
											name="address1" 
											value={this.state.address1}
											onChange={this._updateReceiptState('address1')}
										/>
									</Row>
									<Row label="Address 2" className="no-pad">
										<TextField 
											name="address2" 
											value={this.state.address2}
											onChange={this._updateReceiptState('address2')}
										/>
									</Row>
									<Row label="City" className="no-pad">
										<TextField 
											name="city" 
											value={this.state.city}
											onChange={this._updateReceiptState('city')}
										/>
									</Row>
									<Row label="State" className="no-pad">
										<TextField 
											name="state" 
											value={this.state.state}
											onChange={this._updateReceiptState('state')}
										/>
									</Row>
									<Row label="Zip" className="no-pad">
										<TextField 
											name="zip" 
											value={this.state.zip}
											onChange={this._updateReceiptState('zip')}
										/>
									</Row>
									<Row label="Country" className="no-pad">
										<TextField 
											name="country" 
											value={this.state.country}
											onChange={this._updateReceiptState('country')}
										/>
									</Row>
									<Row label="Country Code" className="no-pad">
										<TextField 
											name="countryCode" 
											value={this.state.countryCode}
											onChange={this._updateReceiptState('countryCode')}
										/>
									</Row>
									<Row label="Courier">
										{this.props.receipt.courier}
									</Row>
									<Row label="Tracking Number">
										{this.props.receipt.trackingNumber}
									</Row>
								</Table>
							</Tab>
			      </Tabs>
					</div>
				</Transaction>
			)}
			</div>
		);
	}
});