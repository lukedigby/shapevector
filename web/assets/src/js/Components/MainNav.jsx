import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';
import {IndexLink, Link} from 'react-router';

export default React.createClass({
	getInitialState: function() {
		this.pathPrefix = window.location.href.indexOf('app_dev.php') != -1 ? '/app_dev.php' : '';
		this.paths = {
			loadNewOrders: this.context.Routing.generate('post_order_request', {}, true),
			config: this.pathPrefix + '/config',
			settings: this.pathPrefix + '/settings',
			updateTrackingInfo: this.context.Routing.generate('updateTrackingInfo', {}, true)
		}
		return {};
	},
	contextTypes: {
		Routing: 	React.PropTypes.object.isRequired,
		showFlash: 	React.PropTypes.func.isRequired
	},

	_loadNewOrders: function() {
		this.context.showFlash('Initialising new order loading...');
		$.ajax({
			url: this.paths.loadNewOrders,
			method: 'POST',
			xhrFields: {
				withCredentials: true
			},
			success: function(data){
				this.context.showFlash('Loading new orders...');
				console.log(data);
			}.bind(this)
		});
	},

	render: function() {
		var navButtonStyle = {
			color: 'rgba(255, 255, 255, 0.701961)',
			margin: '10px 0'
		};
		var toolBarStyle = {
			backgroundColor: 'rgb(0, 188, 212)', 
			boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 3px rgba(0,0,0,0.24)',
			position: 'relative',
			zIndex: '10'
		}; 
		return (
			<Toolbar style={toolBarStyle} className="main-nav">
				<ToolbarGroup style={{width: '100%'}}>
					<FlatButton 
						style={navButtonStyle} 
						label="Shapevector" 
						containerElement={<IndexLink to={this.pathPrefix + "/"} activeClassName="active" />}
					/>
					<FlatButton 
						style={navButtonStyle} 
						label="Load new Orders" 
						onClick={this._loadNewOrders} 
					/>
					<FlatButton 
						style={navButtonStyle} 
						label="Settings" 
						containerElement={<Link to={this.paths.settings} activeClassName="active" />}
					/>
					<FlatButton 
						style={navButtonStyle} 
						label="Configuration" 
						containerElement={<Link to={this.paths.config} activeClassName="active" />} 
					/>
					<FlatButton style={navButtonStyle} label="Update Tracking" href={this.paths.updateTrackingInfo} />
					<FlatButton style={navButtonStyle} label={"Logout " + app.user.username} href={this.pathPrefix + "/logout"} />
				</ToolbarGroup>
			</Toolbar>
		);
	}
})