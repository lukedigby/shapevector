import Chip from 'material-ui/Chip';
import Autocomplete from 'material-ui/Autocomplete';
import TextField from 'material-ui/TextField';

export default React.createClass({
	getInitialState: function(){
		this.dataSourceConfig ={
			text: 'name',
			value: 'id'
		};
		return {
			dataSource: this.props.sourceData,
			searchText: '',
			tags: this.props.tags,
			loadedTags: this.props.loadedTags
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
	  this.setState({
			dataSource: nextProps.sourceData,
			tags: nextProps.tags,
			loadedTags: nextProps.loadedTags
	  });
	},

	_onNewRequest: function(value) {
		if(value.name){
			value = value.name;
		}
		this.props.onRequestAddTag(value);
		this.setState({
			searchText: ''
		});
	},
  _handleUpdateInput: function(text) { 
  	this.setState({ searchText: text }); 
  },
  _onBlurInput: function(e){
  	e.stopPropagation();
  },

	render: function(){
		return (
			<div style={this.props.style}>
				<div className="chips" style={{display: 'flex', flexWrap: 'wrap'}}>
					{this.state.tags.map(function(tag) {
						return (
							<Chip 
								style={{margin: 4}} 
								key={"tag" +tag.id} 
								onRequestDelete={() => this.props.onDeleteTag(tag.id)}
							>
								{tag.name}
							</Chip>
						);
					}, this)}
				</div>
				{this.state.loadedTags 
				? this.props.maxTags && this.props.maxTags == this.state.tags.length 
					? <TextField className="disabled-autocomplete" name="disabled" disabled={true} />
					: <Autocomplete 
						searchText={this.state.searchText}
						hintText="+Tag"
						onUpdateInput={this._handleUpdateInput}
						dataSource={this.state.dataSource} 
						dataSourceConfig={this.dataSourceConfig} 
						onNewRequest={this._onNewRequest}
						onBlur={this._onBlurInput}
						filter={Autocomplete.noFilter}
					/> 
				: ''
				}
			</div>
		);
	}
});