import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';

export default React.createClass({
	render: function(){
		return (
			<RaisedButton 
				style={{whiteSpace: 'nowrap'}} 
				primary={true} 
				label={this.props.ordering ? "Ordering..." : "Place Order"}
				icon={this.props.ordering ? <FontIcon className="fa fa-spin fa-refresh" /> : ''} 
				onTouchTap={this.props.placeShapewaysOrder} 
			/>
		);
	}
});
