export default React.createClass({
	render: function() {
		return (
			<table className="table">
				<colgroup>
					<col width="30%"/>
				</colgroup>
				<tbody>
					{this.props.children}
				</tbody>
			</table>
		);
	}
})