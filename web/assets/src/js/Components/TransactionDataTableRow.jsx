export default React.createClass({
	render: function() {
		return (
			<tr className={this.props.className} >
				<th>{this.props.label}</th>
				<td>{this.props.children}</td>
			</tr>
		);
	}
})