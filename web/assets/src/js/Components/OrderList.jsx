import VirtualList from 'react-virtual-list';
import Receipt from './Receipt';
import TagPanel from './TagPanel';
import update from 'react-addons-update';
import moment from 'moment';
import RaisedButton from 'material-ui/RaisedButton';
import fileDownload from 'react-file-download';

export default React.createClass({
	getInitialState: function() {
		return {
			shapewaysMaterials: [],
			tagSourceData: [],
			loadedTags: false
		};
	},
	componentDidMount: function() {
		this._getTags();
		this._getMaterials();
	},

	_getTags: function() {
		$.ajax(this.props.paths.getTags, {
			success: tags => {
				this.setState({
					tagSourceData: tags,
					loadedTags: true
				});
			},
			xhrFields: {
				withCredentials: true
			}
		});
	},
	_getMaterials: function() {
		$.ajax(this.props.paths.getMaterials, {
			success: data => {
				let materials = JSON.parse(data.materials);
				this.setState({
					shapewaysMaterials: materials,
					loadedTags: true
				});
			},
			xhrFields: {
				withCredentials: true
			}
		});
	},
	_onCreateNewTag: function(tag) {
		this.setState(update(this.state,
		{
			tagSourceData: {$push: [tag]}
		}));
	},
	_renderListItem: function(receipt) {
		return (
			<Receipt 
				key={receipt.orderId} 
				{...this.props}
				receipt={receipt}
				onCreateNewTag={this._onCreateNewTag}
				tagSourceData={this.state.tagSourceData}
				loadedTags={this.state.loadedTags}
				materials={this.state.shapewaysMaterials}
			/>
		);
	},
	_onClickTag: function(id) {
		this.props.onTagFilterChange(id);
	},
	_downloadCSV: function() {
		let csvData = this.props.visibleReceipts.map(receipt => {
			let row = [
				receipt.firstName,
				receipt.lastName,
				receipt.address1 + receipt.address2,
				receipt.city,
				receipt.state,
				receipt.zip,
				receipt.country,
			];

			let items = [];
			let printMessage = [];
			receipt.transactions.forEach(transaction => {
				items.push(transaction.itemName);
				transaction.tags.forEach(id => {
					printMessage.push(this.state.tagSourceData.filter(tag => tag.id == id)[0].name);
				})
			});
			row.push(items.join(' '));
			row.push(printMessage.join(' '));
			row.push(moment().format('MMM D YYYY'));
			row.push(receipt.orderId);
			row.push(receipt.buyerEmail);
			row.push(receipt.trackingNumber);

			return row;
		});

		csvData.unshift([
			"First Name",
			"Last Name",
			"Address",
			"City",
			"State",
			"ZIP",
			"Country",
			"Item Ordered",
			"Print Message",
			"Date",
			"Order ID",
			"Email",
			"Tracking Number"
		]);


		let csv = csvData.map(row => row.map(item => item ? item.toString().replace(/,/g, "") : '').join(','));

		fileDownload(csv.join("\n"), 'export.csv');
	},

	render: function() {
		return (
			<div className="row">
				<div className="col-xs-2">
					<RaisedButton label="download CSV" onClick={this._downloadCSV}/>
					<TagPanel 
						tags={this.state.tagSourceData}
						onClickTag={this._onClickTag}
						selectedTag={this.props.filters.tag}
					/>
				</div>	
				<div className="col-xs-10">
					<div className="orderList">
						{this.props.pagedReceipts.map(this._renderListItem)}
					</div>
				</div>
			</div>
		);
	}
});