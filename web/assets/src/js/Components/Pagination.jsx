export default React.createClass({
	getInitialState: function(){
		this.total_pages = Math.ceil(this.props.totalResults / this.props.pageSize);
		return {};
	},
	componentWillReceiveProps: function(props){
		this.total_pages = Math.ceil(props.totalResults / props.pageSize);
	},
	_onPageChange: function(pageNumber, echo){
		if(pageNumber < 1 || pageNumber > this.total_pages){ return null; }
		return function(){
			this.props.onPageChange(pageNumber);
		}.bind(this);
	},
	render: function() {
		var pageRange = 5;
		
		var calculated_first_page = this.props.pageNumber - Math.floor(pageRange / 2);
	  var last_page_extra = Math.max(0, 1-calculated_first_page);
	  var calculated_last_page = this.props.pageNumber + Math.floor(pageRange / 2);
	  var first_page_extra = Math.max(0, -(this.total_pages - calculated_last_page));

	  var first_page = Math.max(1, this.props.pageNumber - Math.floor(pageRange / 2) - first_page_extra);
	  var last_page = Math.min(this.total_pages, this.props.pageNumber + Math.floor(pageRange / 2) + last_page_extra);

	  var first_result = ((this.props.pageNumber - 1) * this.props.pageSize) + 1;
	  var last_result = Math.min(this.props.totalResults, first_result + this.props.pageSize - 1);


		return (
			<nav>
		    <ul className="pagination">
		      <li className={this.props.pageNumber == 1 ? 'disabled' : ''}>
		        <a onTouchTap={this._onPageChange(1)}>
		          <span aria-hidden="true">First</span>
		        </a>
		      </li>
		      <li className={this.props.pageNumber == 1 ? 'disabled' : ''}>
		        <a onTouchTap={this._onPageChange(this.props.pageNumber - 1)}>
		          <span aria-hidden="true">&laquo;</span>
		        </a>
		      </li>
		      {(()=>{
		      	var pages = [];
		      	for(var i=first_page; i<=last_page;i++){
		      		pages.push(
			      	<li key={i} className={this.props.pageNumber == i ? 'active' : ''}>
			        	<a onTouchTap={this._onPageChange(i)}>{i}</a>
			      	</li>
	      			);
		      	}
	    			return pages;
		      })()}
		      <li className={this.props.pageNumber == this.total_pages ? 'disabled' : ''}>
		        <a onTouchTap={this._onPageChange(this.props.pageNumber + 1, true)}>
		          <span aria-hidden="true">&raquo;</span>
		        </a>
		      </li>
		    </ul>
		    <span>Showing orders {first_result}-{last_result} of {this.props.totalResults}</span>
			</nav>
		);	
	}
});