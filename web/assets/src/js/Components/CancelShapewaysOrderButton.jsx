import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';

export default React.createClass({
	render: function(){
		return (
			<RaisedButton 
				style={{whiteSpace: 'nowrap'}} 
				primary={true} 
				label={this.props.cancelling ? "Cancelling..." : "Cancel Order"}
				icon={this.props.cancelling ? <FontIcon className="fa fa-spin fa-refresh" /> : ''} 
				onTouchTap={this.props.cancelShapewaysOrder} 
			/>
		);
	}
});
