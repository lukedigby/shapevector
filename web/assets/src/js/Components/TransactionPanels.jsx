import moment from 'moment';
import ReactDOM from 'react-dom';
import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from './TransactionDataTableTextField';
import TagList from './TagList';
import Table from './TransactionDataTable';
import Row from './TransactionDataTableRow';
import update from 'react-addons-update';

const keyDetailsModel = {
  modelId: {type: String},
  materialId: {type: String},
  orderNotes: {type: String}
};

export default React.createClass({
	getInitialState: function() {
		return {
			focusedInputValue: '',
			addingTag: false
		}
	},

	defaultProps: {
		extraDetails: []
	},

	_handlePageClick: function(e) {
		this.setState({addingTag: false});
		document.removeEventListener('click', this._handlePageClick);
	},
	_onBlur: function(e) {
		if(e.target.value != this.state.focusedInputValue){
			this.props.saveOrder();
		}
	},
	_onFocus(e) {
		this.setState({
			focusedInputValue: e.target.value
		});
	},

	render: function() {
		return (
			<Tabs onBlur={this._onBlur} onFocus={this._onFocus}>
				<Tab label="Key Details" value={"key" + this.props.id}>
					<Table>
						{/*<Row label="Etsy Username">
							{this.props.receipt.buyerEtsyUsername || ''}
							<a target="_blank" href={"http://www.etsy.com/conversations/new?with_id=" + this.props.receipt.buyerEtsyUsername || ''}>
								Send Etsy Message
							</a>
						</Row>*/}
						<Row label="Order ID">
							{this.props.receipt.orderId}
						</Row>
						<Row label="Model ID" className="no-pad">
							<TextField 
								name="model_id" 
								value={this.props.modelId} 
								onChange={this.props.linkState('modelId')} 
								style={{width: 128}} 
							/>
							{(() => {
								if(this.props.modelId && this.props.modelId != "0"){
									return (
										<a target="_blank" href={"http://www.shapeways.com/model/upload-and-buy/" + this.props.modelId}>
											View Model on Shapeways
										</a>
									);
								} 
							})()}
						</Row>
						<Row label="Material ID" className="no-pad">
								<TextField 
									name="material_id" 
									value={this.props.materialId} 
									onChange={this.props.linkState('materialId')} 
									style={{width: 128}} 
								/>
						</Row>
						<Row label="Shapeways order number">
								{(() => {
									if(this.props.shapewaysOrderNumber && this.props.shapewaysOrderNumber != 0){
										return (
											<span>
												<a target="_blank" href={"https://www.shapeways.com/checkout/receipt?orderId=" + this.props.shapewaysOrderNumber}>
													{this.props.shapewaysOrderNumber}
												</a>
											</span>
										);
									}
								})()} 
						</Row>
						<Row label="Target Ship Date">
							{(()=>{
								if (this.props.targetShippingDate){
									let date = moment.unix(this.props.targetShippingDate).format('ddd, MMM D Y');
									return (
										<span>
											{date}
										</span>
									);
								}
							})()}
						</Row>
						<Row label="Message from buyer">
							{this.props.receipt.buyerMessage}
						</Row>
						<Row label="Item quantity">
							{this.props.itemQuantity}
						</Row>
						{this.props.extraDetails.map((item) => (
							<Row key={item.name+'-'+item.value} label={item.name}>
								{item.value}
							</Row>
						))}
						{/*<Row label="Variations">
							{this.props.itemVariations}
						</Row>*/}
						<Row label="Order Notes" className="no-pad">
							<TextField 
								name="notes" 
								multiLine={true}
								rows={1}
								rowsMax={2} 
								hintText="Write order notes here"
								value={this.props.orderNotes}
								onChange={this.props.linkState('orderNotes')}
							/>
						</Row>
						<Row label="Tags" className="no-pad">
							<TagList onRequestAddTag={this.props.onRequestAddTag} tags={this.props.activeTags} onDeleteTag={this.props.onDeleteTag} sourceData={this.props.tagSourceData} />
						</Row>
					</Table>
				</Tab>
				<Tab label="Order Details" value={"order" + this.props.id}>
					<Table>
						<Row label="Receipt ID">
							{this.props.receipt.id}
						</Row>
						<Row label="Transaction ID">
							{this.props.id}
						</Row>
						<Row label="Order time">
							{moment.unix(this.props.receipt.orderTime).format('ddd, MMM D Y h:mma')}
						</Row>
						<Row label="Item name">
							{this.props.itemName}
						</Row>
					</Table>
				</Tab>
				<Tab label="Address Details" value={"address" + this.props.id}>
					<Table>
						<Row label="Name">
							{this.props.receipt.firstName} {this.props.receipt.lastName}
						</Row>
						<Row label="Address 1" className="no-pad">
							<TextField 
								name="address1" 
								value={this.props.address1}
								onChange={this.props.linkState('address1')}
							/>
						</Row>
						<Row label="Address 2" className="no-pad">
							<TextField 
								name="address2" 
								value={this.props.address2}
								onChange={this.props.linkState('address2')}
							/>
						</Row>
						<Row label="City" className="no-pad">
							<TextField 
								name="city" 
								value={this.props.city}
								onChange={this.props.linkState('city')}
							/>
						</Row>
						<Row label="State" className="no-pad">
							<TextField 
								name="state" 
								value={this.props.state}
								onChange={this.props.linkState('state')}
							/>
						</Row>
						<Row label="Zip" className="no-pad">
							<TextField 
								name="zip" 
								value={this.props.zip}
								onChange={this.props.linkState('zip')}
							/>
						</Row>
						<Row label="Country" className="no-pad">
							<TextField 
								name="country" 
								value={this.props.country}
								onChange={this.props.linkState('country')}
							/>
						</Row>
						<Row label="Country Code" className="no-pad">
							<TextField 
								name="countryCode" 
								value={this.props.countryCode}
								onChange={this.props.linkState('countryCode')}
							/>
						</Row>
						<Row label="Courier">
							{this.props.receipt.courier}
						</Row>
						<Row label="Tracking Number">
							{this.props.receipt.trackingNumber}
						</Row>
					</Table>
				</Tab>
      </Tabs>
		);
	}
});;
