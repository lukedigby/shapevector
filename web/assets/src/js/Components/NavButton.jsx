import FlatButton from 'material-ui/FlatButton';

export default React.createClass({
	render: function() {
		return (
			<FlatButton 
				label={this.props.label} 
				href={this.props.href}
				style={{color: '#fff'}} 
			/>
		);
	}
})