import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';

export default React.createClass({
	getInitialState: function() {
		this.inputStyles = {
	    cursor: 'pointer',
	    position: 'absolute',
	    top: 0,
	    bottom: 0,
	    right: 0,
	    left: 0,
	    width: '100%',
	    opacity: 0,
	  };
		return {
			modelUnit: 'mm',
			selectedFile: '',
			uploading: false
		}
	},

	contextTypes: {
		Routing: 	React.PropTypes.object.isRequired
	},

	_updateModelUnit: function(e, key, payload) {
		console.log(e.target.value);
		this.setState({
			modelUnit: payload
		});
	},
	_onUploadModelClick: function() {
		var fd = new FormData();
		fd.append('file', this.refs.modelFileInput.files[0]);
		fd.append('uploadScale', this.state.modelUnit);

		this.setState({uploading: true});
		console.log('uploading');

		$.ajax({
			url: this.context.Routing.generate('post_shapewaysmodel', {}, true),
			method: 'POST',
			data: fd,
			processData: false,
			contentType: false,
			xhrFields: {
				withCredentials: true
			},
			success: function(data){
				this.props.onUploadModel(data);
				this.refs.modelFileInput.value = '';
				this._onSelectFile();
			}.bind(this),
			complete: function(){
				this.setState({uploading: false});
			}.bind(this)
		});
	},
	_onSelectFile: function() {
		var selectedFile = this.refs.modelFileInput.files[0] ? this.refs.modelFileInput.files[0].name : '';
		this.setState({
			selectedFile: selectedFile
		});
	},

	render: function() {
		return (
			<form 
				action={this.props.url} 
				className="clearfix" 
				encType="multipart/form-data" 
				method="POST"
				style={{marginBottom: '10px'}}
			>
				<SelectField 
					style={{width: '100%'}} 
					value={this.state.modelUnit} 
					onChange={this._updateModelUnit}
					floatingLabelText="Model dimension unit"
				>
		      <MenuItem value="m" primaryText="Metres" />
		      <MenuItem value="mm" primaryText="Millimeters" />
		    </SelectField>
		    {this.state.selectedFile}
				<RaisedButton 
					label={this.state.selectedFile ? "Change File" : "Choose a File"}
					labelPosition="before" 
					style={{
						marginBottom: '10px',
						cursor: 'pointer',
						whiteSpace: 'nowrap'
					}}
				>
					<input ref="modelFileInput" onChange={this._onSelectFile} type="file" style={this.inputStyles}/>
				</RaisedButton>
				<RaisedButton 
					label={this.state.uploading ? "Uploading..." : "Upload Model"} 
      		icon={this.state.uploading ? <FontIcon className="fa fa-spin fa-refresh" /> : ''}					
      		secondary={true} 
					onTouchTap={this._onUploadModelClick}
					style={{whiteSpace: 'nowrap'}}
				/>
			</form>
		);
	}
});