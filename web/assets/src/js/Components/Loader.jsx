export default () => 
	<div id="loading">
  	<div className="center">
  		<p className="message"></p>
  		<i className="fa fa-spin fa-refresh"></i>
		</div>
	</div>;