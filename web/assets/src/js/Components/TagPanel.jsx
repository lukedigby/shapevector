import Chip from 'material-ui/Chip';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import {cyan500} from 'material-ui/styles/colors';

export default React.createClass({
	render: function() {
		return (
			<div>
				<h3>Filter by tag</h3>
				{this.props.selectedTag 
				? <FlatButton 
						label="Clear" 
						onTouchTap={() => this.props.onClickTag(this.props.selectedTag)} 
						icon={<FontIcon className="fa fa-close" />}/>
				: ''}
				{this.props.tags.map(t => {
					return (
						<Chip
							backgroundColor={this.props.selectedTag == t.id ? cyan500 : ''} 
							style={{margin: 4}}
							key={t.id} 
							onTouchTap={() => this.props.onClickTag(t.id)}
						>
							{t.name}
						</Chip> 
					);
				})}
			</div>
		);
	}
})