import TextField from 'material-ui/TextField';
import update from 'react-addons-update';

export default React.createClass({
	getDefaultProps: function() {
		return {
			style: {},
			underlineStyle: {}
		}
	},
	render: function() {
		var style = update({fontSize: 12}, {
			$merge: this.props.style
		})
		var underlineStyle = update({bottom: 14}, {
			$merge: this.props.underlineStyle
		})
		return (
			<TextField
				{...this.props} 
				style={style} 
				underlineStyle={underlineStyle} 
			/>
		);
	}
})