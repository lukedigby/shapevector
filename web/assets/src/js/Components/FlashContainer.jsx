export default React.createClass({
	propTypes: {
		persist: React.PropTypes.bool,
		message: React.PropTypes.string, 
		onClose: React.PropTypes.func 
	},
	render: function() {
		let closeButton = '';
		if(this.props.persist){
			closeButton = 
				<button 
					onClick={this.props.onClose} 
					type="button" 
					className="close" 
				>
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			;
		}
		return (
			<div className="flash-messages">
				{this.props.message != '' ?
				<div className="alert alert-info">
					{closeButton}
					{this.props.message}
				</div> :
				'' }
			</div>
		);
	}
});