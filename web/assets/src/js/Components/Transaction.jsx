import Panels from './TransactionPanels';
import UploadModelForm from './UploadModelForm';
import PlaceShapewaysOrderButton from './PlaceShapewaysOrderButton';
import CancelShapewaysOrderButton from './CancelShapewaysOrderButton';
import Divider from 'material-ui/Divider';
import moment from 'moment';
import shallowCompare from 'react-addons-shallow-compare';
import update from 'react-addons-update';

export default React.createClass({


	shouldComponentUpdate: function(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState);
	},

	render: function() {
		return (
			<div className="row transaction-row">
				{this.props.children}
			</div>
		);
	}
});
