<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160830053348 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE order_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, is_processing TINYINT(1) DEFAULT \'0\' NOT NULL, processing_start_time INT DEFAULT NULL, is_completed TINYINT(1) DEFAULT \'0\' NOT NULL, current_result_count INT DEFAULT NULL, total_result_count INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE receipt (id INT NOT NULL, user_id INT NOT NULL, order_id INT NOT NULL, order_time NUMERIC(20, 0) NOT NULL, buyer_etsy_username VARCHAR(100) NOT NULL, buyer_email VARCHAR(255) NOT NULL, buyer_message LONGTEXT DEFAULT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, address1 VARCHAR(255) NOT NULL, address2 VARCHAR(255) DEFAULT NULL, city VARCHAR(255) NOT NULL, state VARCHAR(255) DEFAULT NULL, zip VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, country_code VARCHAR(255) NOT NULL, shapeways_order_number INT DEFAULT NULL, target_shipping_date DATE DEFAULT NULL, tracking_number VARCHAR(100) DEFAULT NULL, courier VARCHAR(100) DEFAULT NULL, INDEX IDX_5399B645A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction_tag (tag_id INT NOT NULL, transaction_id INT NOT NULL, INDEX IDX_F8CD024ABAD26311 (tag_id), INDEX IDX_F8CD024A2FC0CB0F (transaction_id), PRIMARY KEY(tag_id, transaction_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT NOT NULL, receipt_id INT DEFAULT NULL, model_id INT DEFAULT NULL, material_id INT DEFAULT NULL, order_notes LONGTEXT DEFAULT NULL, item_name VARCHAR(200) NOT NULL, item_quantity INT NOT NULL, item_variations VARCHAR(200) DEFAULT NULL, image_url VARCHAR(1000) NOT NULL, INDEX IDX_723705D12B5CA896 (receipt_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, etsy_consumer_key VARCHAR(255) DEFAULT NULL, etsy_consumer_secret VARCHAR(255) DEFAULT NULL, etsy_token VARCHAR(255) DEFAULT NULL, etsy_token_secret VARCHAR(255) DEFAULT NULL, shapeways_consumer_key VARCHAR(255) DEFAULT NULL, shapeways_consumer_secret VARCHAR(255) DEFAULT NULL, shapeways_token VARCHAR(255) DEFAULT NULL, shapeways_token_secret VARCHAR(255) DEFAULT NULL, username_canonical VARCHAR(191) NOT NULL, email_canonical VARCHAR(191) NOT NULL, UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B645A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE transaction_tag ADD CONSTRAINT FK_F8CD024ABAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transaction_tag ADD CONSTRAINT FK_F8CD024A2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D12B5CA896 FOREIGN KEY (receipt_id) REFERENCES receipt (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D12B5CA896');
        $this->addSql('ALTER TABLE transaction_tag DROP FOREIGN KEY FK_F8CD024ABAD26311');
        $this->addSql('ALTER TABLE transaction_tag DROP FOREIGN KEY FK_F8CD024A2FC0CB0F');
        $this->addSql('ALTER TABLE receipt DROP FOREIGN KEY FK_5399B645A76ED395');
        $this->addSql('DROP TABLE order_request');
        $this->addSql('DROP TABLE receipt');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE transaction_tag');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE fos_user');
    }
}
