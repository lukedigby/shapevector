<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161005060355 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction_tag DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE transaction_tag ADD PRIMARY KEY (transaction_id, tag_id)');
        $this->addSql('ALTER TABLE fos_user DROP shapeways_consumer_key, DROP shapeways_consumer_secret, DROP shopify_consumer_key, DROP shopify_consumer_secret');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user ADD shapeways_consumer_key VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD shapeways_consumer_secret VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD shopify_consumer_key VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD shopify_consumer_secret VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE transaction_tag DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE transaction_tag ADD PRIMARY KEY (tag_id, transaction_id)');
    }
}
